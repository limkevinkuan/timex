return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 32,
  height = 32,
  tilewidth = 16,
  tileheight = 16,
  nextlayerid = 8,
  nextobjectid = 60,
  properties = {
    ["script"] = "Maze.MazeGame"
  },
  tilesets = {
    {
      name = "kenney_16x16",
      firstgid = 1,
      filename = "kenney_16x16.tsx",
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      columns = 64,
      image = "kenney_16x16.png",
      imagewidth = 1024,
      imageheight = 1728,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 6912,
      tiles = {
        {
          id = 192,
          type = "MazeBall",
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 2,
                y = 2,
                width = 12,
                height = 12,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 211,
          type = "MazeWall"
        },
        {
          id = 212,
          type = "MazeWall"
        },
        {
          id = 256,
          type = "MazeKey",
          properties = {
            ["keyid"] = "yellow"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 4,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 257,
          type = "MazeKey",
          properties = {
            ["keyid"] = "green"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 4,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 258,
          type = "MazeKey",
          properties = {
            ["keyid"] = "red"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 4,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 259,
          type = "MazeKey",
          properties = {
            ["keyid"] = "blue"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 4,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 275,
          type = "MazeWall"
        },
        {
          id = 276,
          type = "MazeWall"
        },
        {
          id = 320,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "yellow"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 321,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "green"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 322,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "red"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 323,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "blue"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 384,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureCitrine",
            ["treasurevalue"] = 200
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 384,
              duration = 375
            },
            {
              tileid = 448,
              duration = 125
            }
          }
        },
        {
          id = 385,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureEmerald",
            ["treasurevalue"] = 250
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 385,
              duration = 375
            },
            {
              tileid = 449,
              duration = 125
            }
          }
        },
        {
          id = 386,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureRuby",
            ["treasurevalue"] = 500
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 386,
              duration = 375
            },
            {
              tileid = 450,
              duration = 125
            }
          }
        },
        {
          id = 387,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureDiamond",
            ["treasurevalue"] = 1000
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 387,
              duration = 375
            },
            {
              tileid = 451,
              duration = 125
            }
          }
        },
        {
          id = 398,
          type = "MazeWall"
        },
        {
          id = 448,
          type = "MazeTreasure",
          properties = {
            ["treasurevalue"] = 200
          }
        },
        {
          id = 449,
          type = "MazeTreasure",
          properties = {
            ["treasurevalue"] = 250
          }
        },
        {
          id = 450,
          type = "MazeTreasure",
          properties = {
            ["treasurevalue"] = 500
          }
        },
        {
          id = 451,
          type = "MazeTreasure",
          properties = {
            ["treasurevalue"] = 1000
          }
        },
        {
          id = 512,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_yellow_round_off",
            ["buttonontileid"] = "button_yellow_round_on",
            ["name"] = "button_yellow_round_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 513,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_green_round_off",
            ["buttonontileid"] = "button_green_round_on",
            ["name"] = "button_green_round_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 514,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_red_round_off",
            ["buttonontileid"] = "button_red_round_on",
            ["name"] = "button_red_round_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 515,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_blue_round_off",
            ["buttonontileid"] = "button_blue_round_on",
            ["name"] = "button_blue_round_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 576,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_yellow_round_off",
            ["buttonontileid"] = "button_yellow_round_on",
            ["name"] = "button_yellow_round_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 577,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_green_round_off",
            ["buttonontileid"] = "button_green_round_on",
            ["name"] = "button_green_round_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 578,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_red_round_off",
            ["buttonontileid"] = "button_red_round_on",
            ["name"] = "button_red_round_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 579,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_blue_round_off",
            ["buttonontileid"] = "button_blue_round_on",
            ["name"] = "button_blue_round_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 580,
          type = "MazeBombFire",
          properties = {
            ["name"] = "bombfire"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 2,
                y = 2,
                width = 12,
                height = 12,
                rotation = 0,
                visible = true,
                properties = {
                  ["restitution"] = 1
                }
              }
            }
          }
        },
        {
          id = 640,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_yellow_flat_off",
            ["buttonontileid"] = "button_yellow_flat_on",
            ["name"] = "button_yellow_flat_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 4,
                y = 0,
                width = 8,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 641,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_green_flat_off",
            ["buttonontileid"] = "button_green_flat_on",
            ["name"] = "button_green_flat_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 4,
                y = 0,
                width = 8,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 642,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_red_flat_off",
            ["buttonontileid"] = "button_red_flat_on",
            ["name"] = "button_red_flat_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 4,
                y = 0,
                width = 8,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 643,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_blue_flat_off",
            ["buttonontileid"] = "button_blue_flat_on",
            ["name"] = "button_blue_flat_off"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 4,
                y = 0,
                width = 8,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 644,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureBronze",
            ["treasurevalue"] = 10
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 2,
                y = 2,
                width = 12,
                height = 12,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 645,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureSilver",
            ["treasurevalue"] = 50
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 2,
                y = 2,
                width = 12,
                height = 12,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 646,
          type = "MazeTreasure",
          properties = {
            ["name"] = "TreasureGold"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 2,
                y = 2,
                width = 12,
                height = 12,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 653,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["name"] = "box_silver_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 654,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["boxunlockedtileid"] = "box_silver_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 655,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["name"] = "box_silver_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 656,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["boxunlockedtileid"] = "box_silver_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 657,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["name"] = "box_silver_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 658,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["boxunlockedtileid"] = "box_silver_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 704,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_yellow_flat_off",
            ["buttonontileid"] = "button_yellow_flat_on",
            ["name"] = "button_yellow_flat_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 705,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_green_flat_off",
            ["buttonontileid"] = "button_green_flat_on",
            ["name"] = "button_green_flat_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 706,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_red_flat_off",
            ["buttonontileid"] = "button_red_flat_on",
            ["name"] = "button_red_flat_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 707,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "button_blue_flat_off",
            ["buttonontileid"] = "button_blue_flat_on",
            ["name"] = "button_blue_flat_on"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 708,
          type = "MazeBomb",
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 709,
          type = "MazeBomb",
          properties = {
            ["name"] = "bomb_primed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 709,
              duration = 500
            },
            {
              tileid = 708,
              duration = 500
            }
          }
        },
        {
          id = 710,
          type = "MazeBomb",
          properties = {
            ["name"] = "bomb_detonate"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 709,
              duration = 33
            },
            {
              tileid = 710,
              duration = 33
            }
          }
        },
        {
          id = 717,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["name"] = "box_gold_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 718,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["boxunlockedtileid"] = "box_gold_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 719,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["name"] = "box_gold_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 720,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["boxunlockedtileid"] = "box_gold_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 721,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["name"] = "box_gold_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 722,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["boxunlockedtileid"] = "box_gold_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 774,
          type = "MazeGoal"
        },
        {
          id = 781,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["name"] = "box_iron_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 782,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["boxunlockedtileid"] = "box_iron_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 783,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["name"] = "box_iron_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 784,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["boxunlockedtileid"] = "box_iron_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 785,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["name"] = "box_iron_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 786,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["boxunlockedtileid"] = "box_iron_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 838,
          type = "MazeGoal"
        },
        {
          id = 845,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["name"] = "box_bronze_!"
          }
        },
        {
          id = 846,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["boxunlockedtileid"] = "box_bronze_!"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 847,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["name"] = "box_bronze_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 848,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["boxunlockedtileid"] = "box_bronze_coin"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 849,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["name"] = "box_bronze_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 850,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["boxunlockedtileid"] = "box_bronze_danger"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 902,
          type = "MazeGoal",
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 902,
              duration = 125
            },
            {
              tileid = 903,
              duration = 125
            }
          }
        },
        {
          id = 909,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_!",
            ["name"] = "box_opened_!"
          }
        },
        {
          id = 910,
          type = "MazeBox"
        },
        {
          id = 911,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_coin",
            ["name"] = "box_opened_coin"
          }
        },
        {
          id = 912,
          type = "MazeBox"
        },
        {
          id = 913,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "box_opened_danger",
            ["name"] = "box_opened_danger"
          }
        },
        {
          id = 914,
          type = "MazeBox"
        },
        {
          id = 966,
          type = "MazeGoal",
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 966,
              duration = 125
            },
            {
              tileid = 967,
              duration = 125
            }
          }
        },
        {
          id = 981,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_opened",
            ["static"] = false
          }
        },
        {
          id = 982,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_standard",
            ["static"] = false
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 983,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_damaged",
            ["static"] = false
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 984,
          type = "MazeBox",
          properties = {
            ["boxdamagetileid"] = "crate_damaged",
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_reinforced",
            ["static"] = false
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 1045,
          type = "MazeWall"
        },
        {
          id = 1046,
          type = "MazeWall"
        },
        {
          id = 1047,
          type = "MazeWall"
        },
        {
          id = 1048,
          type = "MazeWall"
        },
        {
          id = 1090,
          type = "MazeSpring",
          properties = {
            ["name"] = "spring_in"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 8,
                width = 16,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 1091,
          type = "MazeSpring"
        },
        {
          id = 1092,
          type = "MazeSpring",
          properties = {
            ["name"] = "spring_out"
          },
          animation = {
            {
              tileid = 1092,
              duration = 67
            },
            {
              tileid = 1091,
              duration = 67
            },
            {
              tileid = 1090,
              duration = 67
            }
          }
        },
        {
          id = 1093,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 7,
                y = 0,
                width = 2,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 1109,
          type = "MazeWall"
        },
        {
          id = 1110,
          type = "MazeWall"
        },
        {
          id = 1111,
          type = "MazeWall"
        },
        {
          id = 1112,
          type = "MazeWall"
        },
        {
          id = 1220,
          animation = {
            {
              tileid = 1220,
              duration = 125
            },
            {
              tileid = 1221,
              duration = 125
            }
          }
        },
        {
          id = 1802,
          type = "MazeWall"
        },
        {
          id = 1803,
          type = "MazeWall"
        },
        {
          id = 1866,
          type = "MazeWall"
        },
        {
          id = 1867,
          type = "MazeWall"
        },
        {
          id = 1930,
          type = "MazeWall"
        },
        {
          id = 1931,
          type = "MazeWall"
        },
        {
          id = 1994,
          type = "MazeWall"
        },
        {
          id = 1995,
          type = "MazeWall"
        },
        {
          id = 2058,
          type = "MazeWall"
        },
        {
          id = 2059,
          type = "MazeWall"
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      id = 1,
      name = "bg",
      x = 0,
      y = 0,
      width = 32,
      height = 32,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt1dlNQmEUhdErhSAyCCi2xCSIILakoAIO2JIMAipDD64euIkh/g/r8Xs6ydnlKIoqVKlR55IGTa5ocU2bDjd0uaUc+tDv0d9xT48+DzzyxIAhI5554ZU3xrzH0H8wYcqMOZ8sWLLii29+WLNhyy6GPnkURcekOCFNhiw5TslToMgZ55S4ONq/L1OhSo06lzRockWLa9p0uKHLbQz9Hff06PPAI08MGDLimRdeeWPMewz9BxOmzJjzyYIlK7745oc1G7bsYuiTCfcjxQlpMmTJcUqeAkXOOKfERSL0od+v/+v/H/r/3Yf9D/sf9v9w9yP0h93/AldZmZA="
    },
    {
      type = "objectgroup",
      id = 2,
      name = "behindmazeobjects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 45,
          name = "",
          type = "",
          shape = "rectangle",
          x = 24,
          y = 184,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 985,
          visible = true,
          properties = {
            ["boxitemcount"] = 5,
            ["boxitemtileid"] = "TreasureSilver"
          }
        }
      }
    },
    {
      type = "tilelayer",
      id = 3,
      name = "maze",
      x = 0,
      y = 0,
      width = 32,
      height = 32,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzVlFsOhSAMREnUPbAhl+a+jR/+EB5nKEhtQi7hDj1pLRP3EOKi9cT7W4uShtxt5fTAr/Un5dA9zanWr+5beTzxaz2cpbHUUApF441P5pZ6C8mpvmHSo5I+d+6JP9KL6N1Rc9a798C3LMKh/kPqVDUj5v8rvtVzyN0WX6mZhDL/3viz3wX5n9ZGznOaP/CtnlP7RqRmcq72yzu/109ymlR/bG3/Vd+/oj8X86+Ev2p5iBvYIiiN"
    },
    {
      type = "objectgroup",
      id = 4,
      name = "mazeobjects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 19,
          name = "",
          type = "",
          shape = "rectangle",
          x = 120,
          y = 136,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 260,
          visible = true,
          properties = {}
        },
        {
          id = 20,
          name = "",
          type = "",
          shape = "rectangle",
          x = 216,
          y = 208,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 324,
          visible = true,
          properties = {}
        },
        {
          id = 24,
          name = "",
          type = "",
          shape = "rectangle",
          x = 408,
          y = 424,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 257,
          visible = true,
          properties = {}
        },
        {
          id = 25,
          name = "",
          type = "",
          shape = "rectangle",
          x = 72,
          y = 232,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 258,
          visible = true,
          properties = {}
        },
        {
          id = 26,
          name = "",
          type = "",
          shape = "rectangle",
          x = 24,
          y = 208,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 323,
          visible = true,
          properties = {}
        },
        {
          id = 28,
          name = "",
          type = "",
          shape = "rectangle",
          x = 72,
          y = 152,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 515,
          visible = true,
          properties = {
            ["buttonfunction"] = "discardObject",
            ["discardobjectid"] = 26
          }
        },
        {
          id = 29,
          name = "",
          type = "",
          shape = "rectangle",
          x = 144,
          y = 184,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 322,
          visible = true,
          properties = {}
        },
        {
          id = 30,
          name = "",
          type = "",
          shape = "rectangle",
          x = 432,
          y = 472,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 321,
          visible = true,
          properties = {}
        },
        {
          id = 31,
          name = "",
          type = "",
          shape = "rectangle",
          x = 168,
          y = 248,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 38,
          name = "",
          type = "",
          shape = "rectangle",
          x = 336,
          y = 440,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 39,
          name = "",
          type = "",
          shape = "rectangle",
          x = 368,
          y = 440,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 40,
          name = "",
          type = "",
          shape = "rectangle",
          x = 400,
          y = 392,
          width = 16,
          height = 16,
          rotation = -180,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 41,
          name = "",
          type = "",
          shape = "rectangle",
          x = 368,
          y = 392,
          width = 16,
          height = 16,
          rotation = -180,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 42,
          name = "",
          type = "",
          shape = "rectangle",
          x = 440,
          y = 424,
          width = 16,
          height = 16,
          rotation = -90,
          gid = 1091,
          visible = true,
          properties = {}
        },
        {
          id = 43,
          name = "",
          type = "",
          shape = "rectangle",
          x = 152,
          y = 88,
          width = 16,
          height = 16,
          rotation = -450,
          gid = 1091,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 5,
      name = "rope",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 48,
          name = "",
          type = "MazeRope",
          shape = "rectangle",
          x = 120,
          y = 32,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1094,
          visible = true,
          properties = {}
        },
        {
          id = 49,
          name = "",
          type = "MazeRope",
          shape = "rectangle",
          x = 120,
          y = 48,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1094,
          visible = true,
          properties = {}
        },
        {
          id = 50,
          name = "",
          type = "MazeRope",
          shape = "rectangle",
          x = 120,
          y = 64,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1094,
          visible = true,
          properties = {}
        },
        {
          id = 46,
          name = "",
          type = "",
          shape = "rectangle",
          x = 120,
          y = 76,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 709,
          visible = true,
          properties = {}
        },
        {
          id = 56,
          name = "",
          type = "MazeRopeJoint",
          shape = "ellipse",
          x = 126,
          y = 14,
          width = 4,
          height = 4,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 57,
          name = "",
          type = "MazeRopeJoint",
          shape = "ellipse",
          x = 126,
          y = 30,
          width = 4,
          height = 4,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 58,
          name = "",
          type = "MazeRopeJoint",
          shape = "ellipse",
          x = 126,
          y = 46,
          width = 4,
          height = 4,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 59,
          name = "",
          type = "MazeRopeJoint",
          shape = "ellipse",
          x = 126,
          y = 62,
          width = 4,
          height = 4,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 6,
      name = "goal",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 2,
          name = "",
          type = "",
          shape = "rectangle",
          x = 456,
          y = 456,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 2147484551,
          visible = true,
          properties = {
            ["nextmap"] = "mainmenu.lua"
          }
        }
      }
    },
    {
      type = "objectgroup",
      id = 7,
      name = "ball",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "ball",
          type = "",
          shape = "rectangle",
          x = 24,
          y = 40,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 193,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
