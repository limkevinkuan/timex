<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="saw" tilewidth="64" tileheight="64" tilecount="4" columns="4">
 <tileoffset x="-32" y="32"/>
 <image source="saw.png" width="256" height="64"/>
 <tile id="0" type="MazeBall">
  <properties>
   <property name="name" value="body"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="8" y="8" width="48" height="48">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="name" value="eyes"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="name" value="pupils"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="name" value="happy"/>
  </properties>
 </tile>
</tileset>
