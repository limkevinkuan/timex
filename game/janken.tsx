<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="janken" tilewidth="80" tileheight="80" tilecount="3" columns="3">
 <tileoffset x="0" y="80"/>
 <properties>
  <property name="commoncollision" type="int" value="0"/>
 </properties>
 <image source="janken.png" width="240" height="80"/>
 <tile id="0">
  <properties>
   <property name="name" value="rock"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="10" y="10" width="60" height="60">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="name" value="scissors"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="name" value="paper"/>
  </properties>
 </tile>
</tileset>
