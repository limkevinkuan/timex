local levity = require "levity"

local MainMenu = class()
function MainMenu:_init(map)
	love.window.setDisplaySleepEnabled(true)
	local pixelWidth = map.width*map.tilewidth
	local pixelHeight = map.height*map.tileheight
	local centerX = pixelWidth/2
	local centerY = pixelHeight/2
	levity.camera:set(centerX, centerY)--, pixelWidth, pixelHeight)
end

function MainMenu:keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
end

return MainMenu
