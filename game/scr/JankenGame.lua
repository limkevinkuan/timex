local levity = require "levity"

local MaxShakes = 6
local ShakeStartDist = 1
local ShakeEndDist = 1/64

local JankenGame = class()
function JankenGame:_init(map)
	self.map = map
	for _,layer in ipairs(map.layers) do
		if layer.objects then
			for i = #layer.objects, 1, -1 do
				local object = layer.objects[i]
				layer.objects[object.name] = object
			end
		end
	end

	self.accelerometerPrev = {0, 0, 0}
	self.accelerometer = {0, 0, 0}
	self.numShakes = 0

	local objects = self.map.layers["result"].objects
	local player = objects["player"]
	local opponent = objects["opponent"]
	self.playerX0, self.playerY0 = player.body:getPosition()
	self.opponentX0, self.opponentY0 = opponent.body:getPosition()

	self:enterMode("opponents")
end

function JankenGame:enterMode(mode)
	self.mode = mode
	local modeLayer = self.map.layers[mode]
	local camera = modeLayer.objects["camera"]
	levity.camera:set(camera.x + camera.width/2, camera.y + camera.height/2,
		camera.width, camera.height)
end

function JankenGame:inputRock()
	self:input("rock")
end

function JankenGame:inputScissors()
	self:input("scissors")
end

function JankenGame:inputPaper()
	self:input("paper")
end

local Choices = { "rock", "paper", "scissors" }

function JankenGame:input(inp)
	self.selected = inp
	self:enterMode("result")

	self.numShakes = 0

	local inp2 = Choices[love.math.random(3)]
	local playerGid = self.map:getTileGid("janken", inp)
	local opponentGid = self.map:getTileGid("janken", inp2)

	local objects = self.map.layers["result"].objects

	local shake = objects.shake
	shake.visible = true
	shake.text = "Shake."

	local result = objects.result
	result.visible = false
	result.text = self:decideResult(inp, inp2)

	local player = objects.player
	player.visible = true
	player:setGid(playerGid, self.map)
	player.body:setPosition(self.playerX0, self.playerY0)
	player.body:setLinearVelocity(0, 0)
	levity.scripts:send(player.id, "setIsLoser", false)
	local opponent = objects.opponent
	opponent.visible = true
	opponent:setGid(opponentGid, self.map)
	opponent.body:setPosition(self.opponentX0, self.opponentY0)
	opponent.body:setLinearVelocity(0, 0)
	levity.scripts:send(opponent.id, "setIsLoser", false)
end

local Results = {
	rock = {
		scissors = "WIN",
		paper = "LOSE"
	},
	paper = {
		scissors = "LOSE",
		rock = "WIN"
	},
	scissors = {
		paper = "WIN",
		rock = "LOSE"
	}
}

function JankenGame:decideResult(inp1, inp2)
	return Results[inp1][inp2] or "DRAW"
end

function JankenGame:joystickaxis(joystick, axis, value)
	self.accelerometer[axis] = self.accelerometer[axis] and value
end

function JankenGame:mousereleased()
	self:touchreleased()
end

function JankenGame:mousemoved(x, y, dx, dy)
	local ax = self.accelerometer[1]
	local ay = self.accelerometer[2]
	self.accelerometer[1] = ax and dx/8
	self.accelerometer[2] = ay and dy/8
end

function JankenGame:beginMove(dt)
	local dx = self.accelerometer[1] - self.accelerometerPrev[1]
	local dy = self.accelerometer[2] - self.accelerometerPrev[2]
	local dz = self.accelerometer[3] - self.accelerometerPrev[3]
	local dist = math.sqrt(dx*dx + dy*dy + dz*dz)
	for i, v in ipairs(self.accelerometer) do
		self.accelerometerPrev[i] = v
	end

	if self.mode == "result" then
		local objects = self.map.layers["result"].objects
		local shake = objects.shake
		if self.numShakes % 2 == 0 and dist >= ShakeStartDist
		or self.numShakes % 2 == 1 and dist < ShakeEndDist then
			self.numShakes = self.numShakes + 1
			shake.text = shake.text.."."
		end

		self.numShakes = math.min(MaxShakes, self.numShakes)
		if self.numShakes >= MaxShakes then
			local result = objects.result
			local player = objects.player
			local opponent = objects.opponent

			shake.visible = false

			if result.visible then
				player.body:setLinearVelocity(0, 0)
				opponent.body:setLinearVelocity(0, 0)
			else
				player.body:applyForce(0, -240)
				opponent.body:applyForce(0, 240)
			end
		end
	elseif self.mode == "waiting" then
		self.waittime = self.waittime + dt
		if math.floor(4*(self.waittime))
		< math.floor(4*(self.waittime + dt))
		then
			local objects = self.map.layers["waiting"].objects
			local title = objects.title
			title.text = title.text..'.'
		end
		if self.waittime > 1 then
			self:enterMode("selection")
		end
	end
end

function JankenGame:playersCollided()
	local objects = self.map.layers["result"].objects
	local result = objects.result
	local player = objects.player
	local opponent = objects.opponent

	if result.visible then
		return
	end

	result.visible = true
	if result.text == "WIN" then
		levity.scripts:send(opponent.id, "setIsLoser", true)
	elseif result.text == "LOSE" then
		levity.scripts:send(player.id, "setIsLoser", true)
	end
end

function JankenGame:keypressed(key)
	if key == "escape" then
		if self.mode == "result" then
			self:enterMode("selection")
		elseif self.mode == "selection" then
			self:enterMode("opponents")
		else
			exitCurrentGame()
		end
	end
end

function JankenGame:touchreleased()
	if self.mode == "result" then
		local objects = self.map.layers["result"].objects
		local result = objects.result
		if result.visible then
			exitCurrentGame()
		end
	end
end

function JankenGame:waitForOpponent(opponent)
	self:enterMode("waiting")
	self.waittime = 0

	local objects = self.map.layers["waiting"].objects
	local title = objects.title
	title.text = string.format("Waiting for\n%s", opponent)
end

function JankenGame:waitForOpponent1()
	self:waitForOpponent("Luke")
end

function JankenGame:waitForOpponent2()
	self:waitForOpponent("Nathan")
end

function JankenGame:waitForOpponent3()
	self:waitForOpponent("Elizabeth")
end

return JankenGame
