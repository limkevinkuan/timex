local levity = require "levity"

local UIActions = {}

function UIActions.startTimeGuessingGame()
	love.window.setDisplaySleepEnabled(false)
	levity:setNextMap("timeguessing.lua")
end

function UIActions.startTimeSettingGame()
	love.window.setDisplaySleepEnabled(false)
	levity:setNextMap("timesetting.lua")
end

function UIActions.startJankenGame()
	love.window.setDisplaySleepEnabled(false)
	levity:setNextMap("janken.lua")
end

function UIActions.startLabyrinthGame()
	love.window.setDisplaySleepEnabled(false)
	levity:setNextMap("maze1.lua")
end

function UIActions.soundOn()
	love.audio.setVolume(0.5)
	levity.scripts:broadcast("startGame")
end

function UIActions.soundOff()
	love.audio.setVolume(0)
	levity.scripts:broadcast("startGame")
end

return UIActions
