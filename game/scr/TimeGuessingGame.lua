local levity = require "levity"

local TimeGuessingGame = class()

local function minuteToAngle(minute)
	return minute * math.pi / 30
end

local function hourToAngle(hour, minute)
	local angle = (hour % 12) * math.pi / 6
	return angle + (minuteToAngle(minute)/12)
end

function TimeGuessingGame:_init(map)
	self.map = map
	for _, tileset in pairs(self.map.tilesets) do
		tileset.image:setFilter("linear", "linear")
	end
	for i, object in pairs(self.map.layers["hands"].objects) do
		self[object.name.."Object"] = object
	end
	for i, object in pairs(self.map.layers["quiz"].objects) do
		self[object.name.."Object"] = object
	end

	local pixelWidth = self.map.width*self.map.tilewidth
	local pixelHeight = self.map.height*self.map.tileheight
	local centerX = pixelWidth/2
	local centerY = pixelHeight/2
	levity.camera:set(centerX, centerY)--, pixelWidth, pixelHeight)

	for _, body in pairs(levity.world:getBodyList()) do
		body:setAngularDamping(32)
		for _, fixture in pairs(body:getFixtureList()) do
			fixture:setMask(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
		end
	end

	local hourJoint = love.physics.newRevoluteJoint(self.hourObject.body,
		self.map.box2d_collision.body, centerX, centerY)

	local minuteJoint = love.physics.newRevoluteJoint(self.minuteObject.body,
		self.map.box2d_collision.body, centerX, centerY)

	local instructions = levity.map.layers["instructions"]
	if instructions then
		instructions.visible = true
		self.nextQuestionTimer = 4
		local quiz = levity.map.layers["quiz"]
		quiz.visible = false
	else
		self:generateNewQuestion(2, 55)
	end
end

function TimeGuessingGame:getQuestionString()
	return string.format("%d:%02d", self.questionHour, self.questionMinute)
end

function TimeGuessingGame:generateNewQuestion(hour, minute)
	local initialHour = hour or love.math.random(1, 12)
	local initialMinute = minute or love.math.random(0, 11) * 5
	self.minuteObject.body:setAngle(minuteToAngle(initialMinute))
	self.hourObject.body:setAngle(hourToAngle(initialHour, initialMinute))

	local deltaMinute = love.math.random(1, 3) * 5
	self.questionMinute = initialMinute + deltaMinute
	self.questionHour = initialHour
	if self.questionMinute >= 60 then
		self.questionHour = (initialHour % 12) + 1
		self.questionMinute = self.questionMinute % 60
	end

	self.questionObject.text = self:getQuestionString()

	self.questionTimer = deltaMinute + 5
	self.nextQuestionTimer = nil

	local quiz = levity.map.layers["quiz"]
	quiz.visible = true
	local instructions = levity.map.layers["instructions"]
	if instructions then
		instructions.visible = false
	end
end

local function getDeltaAngle(a1, a2)
	return math.pi - math.abs(math.abs(a1 - a2) - math.pi)
end

function TimeGuessingGame:touchpressed()
	local instructions = levity.map.layers["instructions"]
	if instructions and instructions.visible then
		self.nextQuestionTimer = 0
		return
	end

	local hourAngle = self.hourObject.body:getAngle()
	local minuteAngle = self.minuteObject.body:getAngle()
	local correctMinuteAngle = minuteToAngle(self.questionMinute)
	local correctHourAngle = hourToAngle(self.questionHour, self.questionMinute)
	local minuteDeltaAngle = getDeltaAngle(correctMinuteAngle, minuteAngle)

	if math.abs(minuteDeltaAngle) <= math.pi / 30 then
		self.nextQuestionTimer = 1
		self.questionObject.text = "Correct!"
		self.minuteObject.body:setAngle(correctMinuteAngle)
		self.hourObject.body:setAngle(correctHourAngle)
	else
		self.questionObject.text =
			self:getQuestionString().."\nTry again..."
	end
end

function TimeGuessingGame:keypressed(key)
	if key == "escape" then
		exitCurrentGame()
	else
		self:touchpressed()
	end
end

function TimeGuessingGame:beginMove(dt)
	if self.nextQuestionTimer then
		self.nextQuestionTimer = self.nextQuestionTimer - dt
		if self.nextQuestionTimer < 0 then
			self:generateNewQuestion()
		end
	else
		local hourAngle = self.hourObject.body:getAngle()
		local minuteAngle = self.minuteObject.body:getAngle()
		local minuteDeltaAngle = dt * math.pi / 30
		local hourDeltaAngle = minuteDeltaAngle / 12

		self.minuteObject.body:setAngle(minuteAngle + minuteDeltaAngle)
		self.hourObject.body:setAngle(hourAngle + hourDeltaAngle)

		self.questionTimer = self.questionTimer - dt
		if self.questionTimer < 0 then
			self.nextQuestionTimer = 1
			self.questionObject.text =
				self:getQuestionString().."\nTry again..."
		end
	end
end

--[[
function TimeGuessingGame:mousemoved(x, y, dx, dy, istouch)
	if love.mouse.isDown(1) or istouch then
		self.hourObject.body:applyLinearImpulse(dx, dy, x-dx, y-dy)
	end
end

function TimeGuessingGame:touchmoved(touch, x, y, dx, dy)
	self:mousemoved(x, y, dx, dy, true)
end

function TimeGuessingGame:endMove(dt)
	local angle = math.deg(self.hourObject.body:getAngle()) % 360
	if 90 <= angle and angle <= 270 then
		self.questionObject.text = "When's SCHOOL?"
	else
		self.questionObject.text = "GOOD!"
	end
end
]]

return TimeGuessingGame
