local levity = require "levity"

local TimeSettingGame = class()

local function minuteToAngle(minute)
	return minute * math.pi / 30
end

local function hourToAngle(hour, minute)
	local angle = (hour % 12) * math.pi / 6
	return angle + (minuteToAngle(minute)/12)
end

function TimeSettingGame:_init(map)
	self.map = map
	for _, tileset in pairs(self.map.tilesets) do
		tileset.image:setFilter("linear", "linear")
	end
	for i, object in pairs(self.map.layers["hands"].objects) do
		self[object.name.."Object"] = object
	end
	for i, object in pairs(self.map.layers["quiz"].objects) do
		self[object.name.."Object"] = object
	end

	local pixelWidth = self.map.width*self.map.tilewidth
	local pixelHeight = self.map.height*self.map.tileheight
	local centerX = pixelWidth/2
	local centerY = pixelHeight/2
	levity.camera:set(centerX, centerY)--, pixelWidth, pixelHeight)

	for _, body in pairs(levity.world:getBodyList()) do
		for _, fixture in pairs(body:getFixtureList()) do
			fixture:setMask(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
		end
	end

	local hourJoint = love.physics.newRevoluteJoint(self.hourObject.body,
		self.map.box2d_collision.body, centerX, centerY)

	local minuteJoint = love.physics.newRevoluteJoint(self.minuteObject.body,
		self.map.box2d_collision.body, centerX, centerY)

	local instructions = levity.map.layers["instructions"]
	if instructions then
		instructions.visible = true
		self.nextQuestionTimer = 4
		local quiz = levity.map.layers["quiz"]
		quiz.visible = false
	else
		self:generateNewQuestion()
	end
end

function TimeSettingGame:getQuestionString()
	return string.format("%d:%02d", self.questionHour, self.questionMinute)
end

function TimeSettingGame:generateNewQuestion(hour, minute)
	local initialHour = hour or love.math.random(1, 12)
	local initialMinute = minute or love.math.random(0, 11) * 5
	self.minuteObject.body:setAngle(minuteToAngle(initialMinute))
	self.hourObject.body:setAngle(hourToAngle(initialHour, initialMinute))

	local deltaMinute = love.math.random(2, 10) * 5
	self.questionMinute = initialMinute + deltaMinute
	self.questionHour = initialHour
	if self.questionMinute >= 60 then
		self.questionHour = (initialHour % 12) + 1
		self.questionMinute = self.questionMinute % 60
	end

	self.questionObject.text = self:getQuestionString()

	--self.questionTimer = deltaMinute + 5
	self.nextQuestionTimer = nil

	self.touch = nil
	self.touchedHand = nil

	local quiz = levity.map.layers["quiz"]
	quiz.visible = true
	local instructions = levity.map.layers["instructions"]
	if instructions then
		instructions.visible = false
	end
end

local function getDeltaAngle(a1, a2)
	return math.pi - math.abs(math.abs(a1 - a2) - math.pi)
end

function TimeSettingGame:findTouchedHand(x, y)
	if self.nextQuestionTimer then
		return
	end

	local touchedHand = nil
	local BBHalfSize = 1
	levity.world:queryBoundingBox(x - BBHalfSize, y - BBHalfSize,
	x + BBHalfSize, y + BBHalfSize, function(fixture)
		if fixture:testPoint(x, y) then
			touchedHand = fixture:getBody():getUserData().object
			return false
		end
		return true
	end)
	return touchedHand
end

function TimeSettingGame:touchpressed(touch, x, y)
	local instructions = levity.map.layers["instructions"]
	if instructions and instructions.visible then
		self.nextQuestionTimer = 0
		return
	end

	if self.touch then
		return
	end

	self.touch = touch

	x, y = levity:screenToCamera(x, y)
	x = x + levity.camera.x
	y = y + levity.camera.y
	self.touchedHand = self:findTouchedHand(x, y)
end

function TimeSettingGame:touchmoved(touch, x, y, dx, dy, pressure)
	if self.touch ~= touch then
		return
	end

	x, y = levity:screenToCamera(x, y)

	if not self.touchedHand then
		self.touchedHand = self:findTouchedHand(x + levity.camera.x, y + levity.camera.y)
	end

	if self.touchedHand then
		x = x - levity.camera.w/2
		y = y - levity.camera.h/2

		local body = self.touchedHand.body
		body:setAngle(math.atan2(x, -y))
	end
end

function TimeSettingGame:touchreleased(touch, x, y, dx, dy, pressure)
	if self.touch ~= touch then
		return
	end

	self.touch = nil
	self.touchedHand = nil

	if self.nextQuestionTimer then
		return
	end

	local hourAngle = self.hourObject.body:getAngle()
	while hourAngle >= 2*math.pi do
		hourAngle = hourAngle - 2*math.pi
	end
	while hourAngle <= -2*math.pi do
		hourAngle = hourAngle + 2*math.pi
	end
	local minuteAngle = self.minuteObject.body:getAngle()
	while minuteAngle >= 2*math.pi do
		minuteAngle = minuteAngle - 2*math.pi
	end
	while minuteAngle <= -2*math.pi do
		minuteAngle = minuteAngle + 2*math.pi
	end
	local correctMinuteAngle = minuteToAngle(self.questionMinute)
	local correctHourAngle = hourToAngle(self.questionHour, 0)
	local minuteDeltaAngle = getDeltaAngle(correctMinuteAngle, minuteAngle)
	local hourDeltaAngle = getDeltaAngle(correctHourAngle, hourAngle)

	if math.abs(minuteDeltaAngle) <= minuteToAngle(1)
	and math.abs(hourDeltaAngle) <= minuteToAngle(2) then
		self.nextQuestionTimer = 1
		self.questionObject.text = "Correct!"
		self.minuteObject.body:setAngle(correctMinuteAngle)
		self.hourObject.body:setAngle(correctHourAngle)
	else
		print(correctHourAngle, hourAngle, hourDeltaAngle)
		print(correctMinuteAngle, minuteAngle, minuteDeltaAngle)
	end
end

function TimeSettingGame:mousepressed(x, y, button)
	if button == 1 then
		self:touchpressed("mouse", x, y, 0, 0, 0)
	end
end

function TimeSettingGame:mousemoved(x, y, dx, dy)
	if love.mouse.isDown(1) then
		self:touchmoved("mouse", x, y, dx, dy, 0)
	end
end

function TimeSettingGame:mousereleased(x, y, button)
	if button == 1 then
		self:touchreleased("mouse", x, y, 0, 0, 0)
	end
end

function TimeSettingGame:keypressed(key)
	if key == "escape" then
		exitCurrentGame()
	end
end

function TimeSettingGame:endMove(dt)
	if self.nextQuestionTimer then
		self.nextQuestionTimer = self.nextQuestionTimer - dt
		if self.nextQuestionTimer < 0 then
			self:generateNewQuestion()
		end
	end
end

return TimeSettingGame
