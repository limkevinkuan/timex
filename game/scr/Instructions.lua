local levity = require "levity"

local Instructions = class()
function Instructions:_init(layer)
	self.properties = layer.properties
end

function Instructions:drawUnder()
	local backcolor = self.properties.backcolor or "#80ff0000"
	levity.setColorARGB(backcolor)
	love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
	love.graphics.setColor(255, 255, 255)
end

return Instructions
