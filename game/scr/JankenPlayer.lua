local levity = require "levity"

local JankenPlayer = class()
function JankenPlayer:_init(object)
	self.isLoser = false
end

function JankenPlayer:beginContact(myfixture, otherfixture, contact)
	levity.scripts:broadcast("playersCollided")
end

function JankenPlayer:setIsLoser(isLoser)
	self.isLoser = isLoser
end

function JankenPlayer:beginDraw()
	if self.isLoser then
		love.graphics.setColor(0x80, 0x80, 0x80, 0xff)
	end
end

function JankenPlayer:endDraw()
	if self.isLoser then
		love.graphics.setColor(0xff, 0xff, 0xff, 0xff)
	end
end

return JankenPlayer
