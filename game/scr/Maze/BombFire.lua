local levity = require "levity"

local BombFire = class()
function BombFire:_init(object)
	self.id = object.id
	local properties = object.properties
	local body = object.body

	local speed = properties.projectilespeed or 60
	local impulsex = speed * (properties.projectiledirx or 0)
	local impulsey = speed * (properties.projectilediry or 1)
	body:setLinearVelocity(impulsex, impulsey)

	local spin = math.rad(properties.projectilespin or 0)
	body:setAngularVelocity(spin)

	self.time = properties.projectilelifetime
end

function BombFire:beginContact(myfixture, otherfixture, contact)
	local otherdata = otherfixture:getBody():getUserData()
	local otherproperties = otherdata and otherdata.properties
	if otherproperties then
		local breakable = otherproperties.breakable
		if breakable == "bomb" then
			levity.scripts:broadcast("objectDropItems", otherdata.id)
			levity:discardObject(otherdata.id)
		end
	end
end

function BombFire:endMove(dt)
	local time = self.time - dt
	if time <= 0 then
		levity:discardObject(self.id)
	end
	self.time = time
end

return BombFire
