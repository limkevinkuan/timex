local levity = require "levity"

local Box = class()
function Box:_init(object)
	self.object = object
	self.properties = object.properties
end
--[[
function Box:preSolve(myfix, otherfix, contact)
	local otherbody = otherfix:getBody()
	local otherdata = otherbody:getUserData()
	local otherproperties = otherdata and otherdata.properties
	local droppedbyid = otherproperties and otherproperties.droppedbyid
	if self.object.id == droppedbyid then
		contact:setEnabled(false)
	end
end
]]
function Box:postSolve(myfix, otherfix, contact, nx, ny, n1, t1, n2, t2)
	local boxopenimpulse = self.properties.boxopenimpulse
	if boxopenimpulse and math.hypot(n1, t1) < boxopenimpulse then
		return
	end

	local tileset = self.object.tile.tileset
	local boxdamagetileid = self.properties.boxdamagetileid
	local boxdamagegid = levity.map:getTileGid(tileset, boxdamagetileid)
	if boxdamagegid then
		self.object:setGid(boxdamagegid)
	else
		self:open()
	end
end

function Box:open()
	local tileset = self.object.tile.tileset
	local boxopenedtileid = self.properties.boxopenedtileid
	local boxopenedgid = levity.map:getTileGid(tileset, boxopenedtileid)
	if boxopenedgid == self.object.gid
	or boxopenedgid == self.object.nextgid
	then
		return
	end

	local boxrefills = self.properties.boxrefills

	if not boxrefills then
		local fadeouttime = self.properties.fadeouttime or 1
		self.properties.fadeouttime = fadeouttime
		self.fadeouttimer = fadeouttime
	end

	levity.bank:play(self.properties.opensound)
	levity.scripts:broadcast("objectDropItems", self.object.id)

	if boxopenedgid then
		self.object:setGid(boxopenedgid)
	end
end

function Box:refill()
	self.object:setTileId(self.properties.boxclosedtileid)
end

function Box:endMove(dt)
	if self.fadeouttimer then
		self.fadeouttimer = self.fadeouttimer - dt
		if self.fadeouttimer < 0 then
			levity:discardObject(self.object.id)
		end
	end
end

function Box:beginDraw()
	if self.fadeouttimer then
		local alpha = 255*self.fadeouttimer/self.properties.fadeouttime
		love.graphics.setColor(0xff,0xff,0xff,alpha)
	end
end

function Box:endDraw()
	if self.fadeouttimer then
		love.graphics.setColor(0xff,0xff,0xff,0xff)
	end
end

return Box
