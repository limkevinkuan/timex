local levity = require "levity"

local Bomb = class()
function Bomb:_init(object)
	self.object = object
	self.properties = object.properties
end

function Bomb:postSolve(myfix, otherfix, contact, nx, ny, n1, t1, n2, t2)
	local bombprimeimpulse = self.properties.bombprimeimpulse
	if bombprimeimpulse and math.hypot(n1, t1) < bombprimeimpulse then
		return
	end

	self:prime()
end

function Bomb:prime()
	if self.time then
		return
	end

	levity.bank:play(self.properties.bombticksound)
	self.time = self.properties.bombfusetime
	self.object:setTileId(self.properties.bombprimedtileid)
	self.object.anitimescale = 1
end

function Bomb:endMove(dt)
	if not self.time then
		return
	end

	local time = self.time - dt
	if time > 0 then
		local bombfusetime = self.properties.bombfusetime
		if time <= 1 then
			self.object.anitimescale = 1.5
		end
	elseif self.time > 0 then
		self:detonate()
	end
	self.time = time
end

function Bomb:detonate()
	self.time = 0
	self.object.anitimescale = 1
	self.object:setTileId(self.properties.bombdetonatetileid)
	levity.bank:play(self.properties.bombdetonatesound)

	local boxid = self.properties.droppedbyid
	if boxid then
		levity.scripts:send(boxid, "refill")
	end

	local bombprojectiletileid = self.properties.bombprojectiletileid
	local bombprojectilecount = self.properties.bombprojectilecount or 0
	if not bombprojectiletileid or bombprojectilecount <= 0 then
		return
	end

	local boxid = self.object.id
	local x, y = self.object.body:getPosition()
	local layer = self.object.layer
	local tileset = self.object.tile.tileset
	local bombprojectilegid = levity.map:getTileGid(tileset, bombprojectiletileid)
	local angle = self.object.body:getAngle()
	local deltaangle = 2*math.pi/bombprojectilecount
	for i = 1, bombprojectilecount do
		local dirx = math.cos(angle)
		local diry = math.sin(angle)
		local projectile = levity.map:newObject(layer)
		projectile.gid = bombprojectilegid
		projectile.x = x
		projectile.y = y
		projectile.rotation = math.deg(self.object.body:getAngle())
		projectile.properties.projectiledirx = dirx
		projectile.properties.projectilediry = diry
		angle = angle + deltaangle
	end
end

function Bomb:loopedAnimation()
	if not self.time then
		return
	end
	if self.time >= 0 then
		levity.bank:play(self.properties.bombticksound)
		return
	end

	self.object.body:setActive(false)
	levity:discardObject(self.object.id)
end

return Bomb
