local levity = require "levity"

local Ball = class()
local AccelerometerScale = 180

function Ball:_init(object)
	self.body = object.body
	self.properties = object.properties
	--self.body:setFixedRotation(true)
	self.forces = {0, 0}
	local cx, cy = self.body:getWorldCenter()
	levity.camera:set(cx, cy, love.graphics.getWidth(), love.graphics.getHeight())

	self.foundkeys = {}

	local x, y = object.body:getPosition()

	local eyes = levity.map:newObject(object.layer)
	eyes.x = x
	eyes.y = y
	eyes.gid = levity.map:getTileGid(object.tile.tileset, "eyes")

	local pupils = levity.map:newObject(object.layer)
	pupils.x = x
	pupils.y = y
	pupils.gid = levity.map:getTileGid(object.tile.tileset, "pupils")

	self.faceobjectids = {
		eyes = eyes.id,
		pupils = pupils.id
	}
end

function Ball:setForce(axis, force)
	self.forces[axis] = self.forces[axis] and force
end

function Ball:joystickaxis(joystick, axis, value)
	if self.exitx and self.exity then
		return
	end
	local soundlayer = levity.map.layers.sound
	if soundlayer and soundlayer.visible then
		return
	end
	self:setForce(axis, value*AccelerometerScale)
end

function Ball:mousemoved(x, y, dx, dy)
	if self.exitx and self.exity then
		return
	end
	local soundlayer = levity.map.layers.sound
	if soundlayer and soundlayer.visible then
		return
	end
	x, y = levity:screenToCamera(x, y)
	x = x + levity.camera.x
	y = y + levity.camera.y
	local cx, cy = self.body:getWorldCenter()
	self:setForce(1, x - cx)
	self:setForce(2, y - cy)
end

function Ball:beginMove(dt)
	local SnapToExitSpeed = 16
	if self.exitx and self.exity then
		local cx, cy = self.body:getWorldCenter()
		self.body:setLinearVelocity(
			SnapToExitSpeed*(self.exitx - cx),
			SnapToExitSpeed*(self.exity - cy))
	end
	self.body:applyForce(self.forces[1], self.forces[2])
end

function Ball:beginContact(myfix, otherfix, contact)
	local otherdata = otherfix:getBody():getUserData()
	if not otherdata then
		return
	end
	local otherfixdata = myfix:getUserData()
	if otherfixdata and otherfixdata.properties.reacttocollision == false then
		return
	end
	local otherid = otherdata.id
	local otherproperties = otherdata.properties

	local lockkeyid = otherproperties.lockkeyid
	if lockkeyid and lockkeyid ~= "" then
		local numkeys = self.foundkeys[lockkeyid]
		if numkeys and numkeys > 0 then
			numkeys = numkeys - 1
			self.foundkeys[lockkeyid] = numkeys
			levity.scripts:broadcast("setInventoryItem", lockkeyid, numkeys > 0)
			levity.bank:play(otherproperties.opensound)
			levity:discardObject(otherid)
		else
			return
		end
	end

	local treasurevalue = otherproperties.treasurevalue
	if type(treasurevalue) == "number" then
		levity.scripts:broadcast("addTreasure", treasurevalue)
		levity.scripts:broadcast("objectPlayRandomSound", otherid, "pickupsound")
		levity:discardObject(otherid)
	end

	local keyid = otherproperties.keyid
	if keyid then
		self.foundkeys[keyid] = (self.foundkeys[keyid] or 0) + 1
		levity.scripts:broadcast("setInventoryItem", keyid, true)
		levity.scripts:broadcast("objectPlayRandomSound", otherid, "pickupsound")
		levity:discardObject(otherid)
	end

	local nextmap = otherproperties.nextmap
	if nextmap then
		self.exitx, self.exity = otherfix:getBody():getWorldCenter()

		local eyes = self.faceobjectids.eyes
		eyes = eyes and levity.map.objects[eyes]
		if eyes then
			eyes:setTileId("happy")
		end
		local pupils = self.faceobjectids.pupils
		pupils = pupils and levity.map.objects[pupils]
		if pupils then
			pupils.visible = false
		end
		levity.bank:play(self.properties.winsound)
		levity.scripts:broadcast("playerWon", nextmap)
	end
end

local MaxPupilsOffset = 4
local PupilsOffsetScale = 15
function Ball:endMove(dt)
	local x, y = self.body:getPosition()
	for name, id in pairs(self.faceobjectids) do
		local object = levity.map.objects[id]
		if object then
			local ox, oy = 0, 0
			if name == "pupils" then
				ox = self.forces[1]/PupilsOffsetScale
				oy = self.forces[2]/PupilsOffsetScale
				ox = math.max(-MaxPupilsOffset, math.min(ox, MaxPupilsOffset))
				oy = math.max(-MaxPupilsOffset, math.min(oy, MaxPupilsOffset))
			end
			object.body:setPosition(x+ox, y+oy)
		end
	end
	local cx, cy = self.body:getWorldCenter()
	levity.camera:set(cx, cy)
	local bg = levity.map.layers["bg"]
	if bg then
		bg.offsetx = levity.camera.x
		bg.offsety = levity.camera.y
	end
end

return Ball
