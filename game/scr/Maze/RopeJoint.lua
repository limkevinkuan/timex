local levity = require "levity"

local RopeJoint = class()
function RopeJoint:_init(object)
	self.id = object.id
	self.body = object.body
	self.ropebody1 = nil
	self.ropebody2 = nil
	self.ropefixture1 = nil
	self.ropefixture2 = nil
end

function RopeJoint:beginContact(myfix, otherfix, contact)
	if not self.ropebody1 then
		self.ropefixture1 = otherfix
		self.ropebody1 = otherfix:getBody()
	elseif not self.ropebody2 and otherfix:getBody() ~= self.ropebody1 then
		self.ropefixture2 = otherfix
		self.ropebody2 = otherfix:getBody()
	end
end

function RopeJoint:endMove(dt)
	if self.ropefixture1 and self.ropefixture2 then
		local x, y = self.body:getWorldCenter()
		self.ropefixture1:setSensor(false)
		self.ropefixture2:setSensor(false)
		local ropebody1 = self.ropefixture1:getBody()
		local ropebody2 = self.ropefixture2:getBody()
		love.physics.newRopeJoint(ropebody1, ropebody2, x, y, x, y,
			1, false)
		levity:discardObject(self.id)
	end
end

return RopeJoint
