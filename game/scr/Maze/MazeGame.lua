local levity = require "levity"

local MazeGame = class()
function MazeGame:_init(map)
	local hudlayer = levity.map.layers.hud
	if hudlayer and hudlayer.objects then
		for i = 1, #hudlayer.objects do
			local object = hudlayer.objects[i]
			hudlayer.objects[object.name] = object
		end
		hudlayer.objects.win.visible = false
		hudlayer.objects.treasure.visible = false
		hudlayer.objects.yellowkey.visible = false
		hudlayer.objects.redkey.visible = false
		hudlayer.objects.greenkey.visible = false
		hudlayer.objects.bluekey.visible = false
	end

	local allclearlayer = levity.map.layers.allclear
	if allclearlayer then
		allclearlayer.visible = false
		if allclearlayer.objects then
			for i = 1, #allclearlayer.objects do
				local object = allclearlayer.objects[i]
				allclearlayer.objects[object.name] = object
			end
		end
	end

	local soundlayer = levity.map.layers.sound
	if not soundlayer or not soundlayer.visible then
		self:startGame()
	end
	self.treasurevalue = 0
	self:addTreasure(0)

	local nextmapdata = levity.nextmapdata or {}
	self.totaltime = nextmapdata.totaltime or 0
	self.totaltreasure = nextmapdata.totaltreasure or 0
end

local TimerFormat = "%d:%02d.%03d"
local TreasureFormat = "$%d"

function MazeGame:startGame()
	self.starttime = love.timer.getTime()
	local soundlayer = levity.map.layers.sound
	if soundlayer then
		soundlayer.visible = false
	end

	local music = levity.bank:play(levity.map.properties.ambientsound)
	if music then
		music:setLooping(true)
	end
end

function MazeGame:keypressed(key)
	if key == "escape" then
		love.audio.setVolume(0.5)
		exitCurrentGame()
	end
end

function MazeGame:mousereleased()
	self:touchreleased()
end

function MazeGame:touchreleased()
	local nextmap = levity.map.properties.nextmap
	if nextmap then
		if nextmap == "" then
			levity.map.properties.nextmap = "mainmenu.lua"
			local hudlayer = levity.map.layers.hud
			if hudlayer then
				hudlayer.visible = false
			end
			local allclearlayer = levity.map.layers.allclear
			if allclearlayer then
				allclearlayer.visible = true
				local objects = allclearlayer.objects
				self:updateTimeDisplay(objects.totaltime,
							self.totaltime)
				local treasuretext = TreasureFormat:format(
							self.totaltreasure)
				objects.totaltreasure.text = treasuretext
			end
		elseif love.filesystem.exists(nextmap) then
			levity:setNextMap(nextmap, {
				totaltime = self.totaltime,
				totaltreasure = self.totaltreasure
			})
		else
			love.event.quit()
		end
	end
end

function MazeGame:objectDropItems(droppingobjectid)
	local object = levity.map.objects[droppingobjectid]
	if not object then
		return
	end
	local dropitemtileset = object.properties.dropitemtileset
	if not dropitemtileset or dropitemtileset == "" then
		dropitemtileset = object.tile.tileset
	end
	local dropitemtileid = object.properties.dropitemtileid
	local dropitemcount = object.properties.dropitemcount or 0
	local dropitemgid = levity.map:getTileGid(dropitemtileset, dropitemtileid)
	if not dropitemgid or dropitemcount <= 0 then
		return
	end

	local boxrefills = object.properties.boxrefills
	local layer = object.layer
	local angle = 1.5*math.pi + object.body:getAngle()
	local x, y = object.body:getWorldCenter()
	local deltaangle = 2*math.pi/dropitemcount
	local dropitemoffset = (dropitemcount-1)*4
	for i = 1, dropitemcount do
		local item = levity.map:newObject(layer)
		item.gid = dropitemgid
		item.x = x + dropitemoffset*math.cos(angle)
		item.y = y + dropitemoffset*math.sin(angle)
		if boxrefills then
			item.properties.droppedbyid = droppingobjectid
		end
		angle = angle + deltaangle
	end
	levity.bank:play(object.properties.dropitemsound)
end

local MaxRandomSounds = 8
function MazeGame:objectPlayRandomSound(objectid, prefix)
	local object = levity.map.objects[objectid]
	if not object then
		return
	end
	local properties = object.properties
	local numrandomsounds = 0
	while numrandomsounds < MaxRandomSounds do
		if properties[prefix..numrandomsounds+1] then
			numrandomsounds = numrandomsounds+1
		else
			break
		end
	end
	if numrandomsounds > 0 then
		local randomsound = properties[prefix..love.math.random(numrandomsounds)]
		levity.bank:play(randomsound)
	end
end

function MazeGame:setInventoryItem(itemname, owned)
	local hudlayer = levity.map.layers.hud
	if hudlayer then
		local item = hudlayer.objects[itemname]
		if item then
			item.visible = owned
		end
	end
end

function MazeGame:addTreasure(value)
	self.treasurevalue = self.treasurevalue + value

	local hudlayer = levity.map.layers.hud
	if hudlayer then
		local treasureobject = hudlayer.objects.treasure
		if treasureobject then
			treasureobject.text = TreasureFormat:format(self.treasurevalue)
		end
	end
end

function MazeGame:playerWon(nextmap)
	if levity.map.properties.nextmap then
		return
	end
	levity.map.properties.nextmap = nextmap

	local hudlayer = levity.map.layers.hud
	if hudlayer and hudlayer.objects then
		hudlayer.objects.win.visible = true
		hudlayer.objects.treasure.visible = true
	end
	self.wintime = love.timer.getTime()
	self.totaltime = self.totaltime + self.wintime - self.starttime
	self.totaltreasure = self.totaltreasure + self.treasurevalue
end

function MazeGame:endMove(dt)
	local hudlayer = levity.map.layers.hud
	if hudlayer then
		local timedisplay = hudlayer.objects.time
		if timedisplay then
			local time = 0
			if self.starttime then
				time = (self.wintime or love.timer.getTime()) - self.starttime
			end
			self:updateTimeDisplay(timedisplay, time)
		end
	end
end

function MazeGame:updateTimeDisplay(timedisplay, seconds)
	local minutes = math.floor(seconds/60)
	local msecs = seconds*1000%1000
	seconds = seconds % 60
	timedisplay.text = TimerFormat:format(minutes, seconds, msecs)
end

function MazeGame:beginDraw()
	local hudlayer = levity.map.layers.hud
	if hudlayer then
		hudlayer.offsetx = levity.camera.x
		hudlayer.offsety = levity.camera.y
	end
	local allclearlayer = levity.map.layers.allclear
	if allclearlayer then
		allclearlayer.offsetx = levity.camera.x
		allclearlayer.offsety = levity.camera.y
	end
	local soundlayer = levity.map.layers.sound
	if soundlayer then
		soundlayer.offsetx = levity.camera.x
		soundlayer.offsety = levity.camera.y
	end
end

return MazeGame
