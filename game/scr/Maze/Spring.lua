local levity = require "levity"

local Spring = class()
function Spring:_init(object)
	self.object = object
	self.body = object.body
	self.properties = object.properties
end

function Spring:endMove(dt)
	local body = self.body
	local cx, cy = body:getWorldCenter()
	local contacts = body:getContactList()
	local springimpulse = self.properties.springimpulse or 60
	local angle0 = math.rad(self.properties.angle0 or 0)
	local springangle = angle0 + self.object.body:getAngle()
	local impulsex = (springimpulse * math.cos(springangle))
	local impulsey = (springimpulse * math.sin(springangle))

	local numcontacts = 0
	for i, contact in pairs(contacts) do
		if contact:isTouching() then
			numcontacts = numcontacts + 1
			local myfix, otherfix = contact:getFixtures()
			if myfix:getBody() ~= body then
				myfix, otherfix = otherfix, myfix
			end

			local otherbody = otherfix:getBody()
			otherbody:setPosition(cx, cy)
			otherbody:setAngle(springangle)
			otherbody:setLinearVelocity(impulsex, impulsey)
		end
	end
	if numcontacts > 0 then
		self.object:setTileId(self.properties.springouttileid)
	end
end

function Spring:loopedAnimation()
	self.object:setTileId(self.properties.springintileid)
end

return Spring
