local levity = require "levity"

local Button = class()
function Button:_init(object)
	self.object = object
	self.properties = object.properties
end

function Button:beginContact(myfix, otherfix, contact)
	local fixdata = myfix:getUserData()
	if fixdata and fixdata.properties.reacttocollision == false then
		return
	end

	self:turnOn()
end

local ButtonParams = {}
local MaxParams = 8;
function Button:turnOn()
	local buttonfunction = self.properties.buttonfunction
	if type(self[buttonfunction]) == "function" then
		for i = MaxParams, 1, -1 do
			ButtonParams[i] = nil
		end
		for i = 1, MaxParams do
			ButtonParams[i] = self.properties["functionarg"..i]
		end
		self[buttonfunction](self, unpack(ButtonParams))
	end

	levity.bank:play(self.properties.buttonsound)
	self.object:setTileId(self.properties.buttonontileid)
end

function Button:discardObject()
	levity:discardObject(self.properties.discardobjectid)
end

function Button:primeBomb(bombid)
	levity.scripts:send(bombid, "prime")
end

function Button:detonateBomb(bombid)
	levity.scripts:send(bombid, "detonate")
end

return Button
