<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="timesetting_hands" tilewidth="30" tileheight="192" tilecount="2" columns="2">
 <tileoffset x="-15" y="32"/>
 <image source="timesetting_hands.png" width="60" height="192"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="5" y="0" width="20" height="150">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="5" y="60" width="20" height="90">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
