return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.3.5",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 20,
  height = 20,
  tilewidth = 64,
  tileheight = 64,
  nextlayerid = 6,
  nextobjectid = 94,
  properties = {
    ["overlaymap"] = "mazeui.lua"
  },
  tilesets = {
    {
      name = "spritesheet_tiles",
      firstgid = 1,
      filename = "spritesheet_tiles.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "spritesheet_tiles.png",
      imagewidth = 512,
      imageheight = 1024,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 128,
      tiles = {
        {
          id = 0,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "dangerbox",
            ["boxopenedtileid"] = "dangerboxopened",
            ["name"] = "dangerboxopened"
          }
        },
        {
          id = 4,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 22,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 5,
          animation = {
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 124,
              duration = 100
            }
          }
        },
        {
          id = 6,
          type = "MazeBomb",
          properties = {
            ["name"] = "bomb_detonate"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 6,
              duration = 66
            },
            {
              tileid = 14,
              duration = 66
            },
            {
              tileid = 22,
              duration = 66
            },
            {
              tileid = 30,
              duration = 66
            },
            {
              tileid = 38,
              duration = 66
            }
          }
        },
        {
          id = 8,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "dangerbox",
            ["boxopenedtileid"] = "dangerboxopened",
            ["name"] = "dangerbox"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 12,
          type = "MazeWall",
          properties = {
            ["breakable"] = "bomb"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 20,
          type = "MazeWall"
        },
        {
          id = 21,
          type = "MazeTreasure",
          properties = {
            ["name"] = "silver",
            ["pickupsound1"] = "snd/coin1.ogg",
            ["pickupsound2"] = "snd/coin2.ogg",
            ["pickupsound3"] = "snd/coin3.ogg",
            ["pickupsound4"] = "snd/coin4.ogg",
            ["pickupsound5"] = "snd/coin5.ogg",
            ["pickupsound6"] = "snd/coin6.ogg",
            ["pickupsound7"] = "snd/coin7.ogg",
            ["pickupsound8"] = "snd/coin8.ogg",
            ["treasurevalue"] = 50
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 24,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_damaged",
            ["static"] = false
          }
        },
        {
          id = 25,
          animation = {
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 28,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "specialboxstatic",
            ["boxopenedtileid"] = "specialboxstaticopened",
            ["name"] = "specialboxstaticopened"
          }
        },
        {
          id = 29,
          animation = {
            {
              tileid = 29,
              duration = 100
            },
            {
              tileid = 117,
              duration = 100
            }
          }
        },
        {
          id = 32,
          type = "MazeBox",
          properties = {
            ["boxdamagetileid"] = "crate_damaged",
            ["boxopenedtileid"] = "crate_opened",
            ["name"] = "crate_reinforced",
            ["static"] = false
          }
        },
        {
          id = 33,
          animation = {
            {
              tileid = 33,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            }
          }
        },
        {
          id = 36,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "specialbox",
            ["boxopenedtileid"] = "specialboxopened",
            ["name"] = "specialboxopened"
          }
        },
        {
          id = 37,
          type = "MazeTreasure",
          properties = {
            ["name"] = "copper",
            ["pickupsound1"] = "snd/coin1.ogg",
            ["pickupsound2"] = "snd/coin2.ogg",
            ["pickupsound3"] = "snd/coin3.ogg",
            ["pickupsound4"] = "snd/coin4.ogg",
            ["pickupsound5"] = "snd/coin5.ogg",
            ["pickupsound6"] = "snd/coin6.ogg",
            ["pickupsound7"] = "snd/coin7.ogg",
            ["pickupsound8"] = "snd/coin8.ogg",
            ["treasurevalue"] = 25
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 40,
          type = "MazeBox",
          properties = {
            ["boxopenedtileid"] = "crate_opened",
            ["collidable"] = false,
            ["name"] = "crate_opened",
            ["static"] = false
          }
        },
        {
          id = 41,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "yellowbutton",
            ["buttonontileid"] = "yellowbuttonpressed",
            ["name"] = "yellowbuttonpressed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 44,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "specialboxstatic",
            ["boxopenedtileid"] = "specialboxstaticopened",
            ["name"] = "specialboxstatic"
          }
        },
        {
          id = 45,
          type = "MazeTreasure",
          properties = {
            ["name"] = "star",
            ["pickupsound1"] = "snd/star.ogg",
            ["treasurevalue"] = 1000
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 46,
          type = "MazeBombFire",
          properties = {
            ["name"] = "bombfire"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 46,
              duration = 66
            },
            {
              tileid = 54,
              duration = 66
            },
            {
              tileid = 62,
              duration = 66
            },
            {
              tileid = 70,
              duration = 66
            },
            {
              tileid = 78,
              duration = 66
            }
          }
        },
        {
          id = 48,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "coinboxstatic",
            ["boxopenedtileid"] = "coinboxstaticopened",
            ["name"] = "coinboxstaticopened"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 49,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "yellowbutton",
            ["buttonontileid"] = "yellowbuttonpressed",
            ["name"] = "yellowbutton"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 10,
                y = 22,
                width = 42,
                height = 42,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 52,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "specialbox",
            ["boxopenedtileid"] = "specialboxopened",
            ["name"] = "specialbox",
            ["static"] = false
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 53,
          type = "MazeKey",
          properties = {
            ["keyid"] = "yellowkey",
            ["name"] = "yellowkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 12,
                y = 20,
                width = 40,
                height = 24,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 56,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "coinbox",
            ["boxopenedtileid"] = "coinboxopened",
            ["name"] = "coinboxopened",
            ["static"] = false
          }
        },
        {
          id = 57,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "redbutton",
            ["buttonontileid"] = "redbuttonpressed",
            ["name"] = "redbuttonpressed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 61,
          type = "MazeKey",
          properties = {
            ["keyid"] = "redkey",
            ["name"] = "redkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 12,
                y = 20,
                width = 40,
                height = 24,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 64,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "coinboxstatic",
            ["boxopenedtileid"] = "coinboxstaticopened",
            ["dropitemsound"] = "snd/dropcoins.ogg",
            ["name"] = "coinboxstatic"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 65,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "redbutton",
            ["buttonontileid"] = "redbuttonpressed",
            ["name"] = "redbutton"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 10,
                y = 22,
                width = 42,
                height = 42,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 69,
          type = "MazeKey",
          properties = {
            ["keyid"] = "greenkey",
            ["name"] = "greenkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 12,
                y = 20,
                width = 40,
                height = 24,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 72,
          type = "MazeBox",
          properties = {
            ["boxclosedtileid"] = "coinbox",
            ["boxopenedtileid"] = "coinboxopened",
            ["dropitemsound"] = "snd/dropcoins.ogg",
            ["name"] = "coinbox",
            ["static"] = false
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 73,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "greenbutton",
            ["buttonontileid"] = "greenbuttonpressed",
            ["name"] = "greenbuttonpressed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 74,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "yellowkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 75,
          type = "MazeGoal",
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 30,
                y = 0,
                width = 4,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 76,
          animation = {
            {
              tileid = 76,
              duration = 100
            },
            {
              tileid = 84,
              duration = 100
            }
          }
        },
        {
          id = 77,
          type = "MazeKey",
          properties = {
            ["keyid"] = "bluekey",
            ["name"] = "bluekey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 12,
                y = 20,
                width = 40,
                height = 24,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 80,
          type = "MazeBomb",
          properties = {
            ["name"] = "bomb_primed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 6,
                y = 6,
                width = 52,
                height = 52,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          },
          animation = {
            {
              tileid = 80,
              duration = 100
            },
            {
              tileid = 88,
              duration = 400
            }
          }
        },
        {
          id = 81,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "greenbutton",
            ["buttonontileid"] = "greenbuttonpressed",
            ["name"] = "greenbutton"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 10,
                y = 22,
                width = 42,
                height = 42,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 82,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "redkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 84,
          animation = {
            {
              tileid = 84,
              duration = 100
            },
            {
              tileid = 76,
              duration = 100
            }
          }
        },
        {
          id = 85,
          type = "MazeTreasure",
          properties = {
            ["name"] = "citrine",
            ["treasurevalue"] = 200
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 88,
          type = "MazeBomb",
          properties = {
            ["name"] = "bomb"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 6,
                y = 6,
                width = 52,
                height = 52,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 89,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "bluebutton",
            ["buttonontileid"] = "bluebuttonpressed",
            ["name"] = "bluebuttonpressed"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 90,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "greenkey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 93,
          type = "MazeTreasure",
          properties = {
            ["name"] = "ruby",
            ["treasurevalue"] = 400
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 97,
          type = "MazeButton",
          properties = {
            ["buttonofftileid"] = "bluebutton",
            ["buttonontileid"] = "bluebuttonpressed",
            ["name"] = "bluebutton"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 10,
                y = 22,
                width = 42,
                height = 42,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 2,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 50,
                width = 64,
                height = 14,
                rotation = 0,
                visible = true,
                properties = {
                  ["reacttocollision"] = false
                }
              }
            }
          }
        },
        {
          id = 98,
          type = "MazeLock",
          properties = {
            ["lockkeyid"] = "bluekey"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 64,
                height = 64,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 100,
          animation = {
            {
              tileid = 100,
              duration = 100
            },
            {
              tileid = 108,
              duration = 100
            }
          }
        },
        {
          id = 101,
          type = "MazeTreasure",
          properties = {
            ["name"] = "emerald",
            ["treasurevalue"] = 600
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 105,
          type = "MazeSpring",
          properties = {
            ["name"] = "spring_out"
          },
          animation = {
            {
              tileid = 105,
              duration = 100
            }
          }
        },
        {
          id = 108,
          animation = {
            {
              tileid = 108,
              duration = 100
            },
            {
              tileid = 100,
              duration = 100
            }
          }
        },
        {
          id = 109,
          type = "MazeTreasure",
          properties = {
            ["name"] = "sapphire",
            ["treasurevalue"] = 800
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 113,
          type = "MazeSpring",
          properties = {
            ["name"] = "spring_in"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 12,
                y = 30,
                width = 40,
                height = 4,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 117,
          animation = {
            {
              tileid = 117,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 123,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "rectangle",
                x = 0,
                y = 1,
                width = 64,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 124,
          animation = {
            {
              tileid = 124,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            }
          }
        },
        {
          id = 125,
          type = "MazeTreasure",
          properties = {
            ["name"] = "gold",
            ["pickupsound1"] = "snd/coin1.ogg",
            ["pickupsound2"] = "snd/coin2.ogg",
            ["pickupsound3"] = "snd/coin3.ogg",
            ["pickupsound4"] = "snd/coin4.ogg",
            ["pickupsound5"] = "snd/coin5.ogg",
            ["pickupsound6"] = "snd/coin6.ogg",
            ["pickupsound7"] = "snd/coin7.ogg",
            ["pickupsound8"] = "snd/coin8.ogg",
            ["treasurevalue"] = 100
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["sensor"] = true
                }
              }
            }
          }
        }
      }
    },
    {
      name = "saw",
      firstgid = 129,
      filename = "saw.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      columns = 4,
      image = "saw.png",
      imagewidth = 256,
      imageheight = 64,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 4,
      tiles = {
        {
          id = 0,
          type = "MazeBall",
          properties = {
            ["name"] = "body"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "CollisionFixture",
                shape = "ellipse",
                x = 8,
                y = 8,
                width = 48,
                height = 48,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 1,
          properties = {
            ["name"] = "eyes"
          }
        },
        {
          id = 2,
          properties = {
            ["name"] = "pupils"
          }
        },
        {
          id = 3,
          properties = {
            ["name"] = "happy"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      id = 2,
      name = "bg",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "blue_grass.png",
      properties = {}
    },
    {
      type = "tilelayer",
      id = 1,
      name = "maze",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYKA+EKUSRjYvhApuQmbHUNk8UVwKKTCPknBDNw8EXIDYloBb0eXwmYfsVmzuxqYfn3n4/IDLPeSYR4o7cbkDnzgx5lEar+SkP2LCE5c6fOLY3EEovVAjPRPrNkLqKc272Mwj1T3o8tQsD4hNH+QAAEBLDdQ="
    },
    {
      type = "tilelayer",
      id = 5,
      name = "mazedecoration",
      x = 0,
      y = 0,
      width = 20,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJy9lMENgCAMRTsQN449sogDcOxAjsJoYoIRy2/EhPpOWGry299CRCR0kwjDIJaN3JVEou1LftVf2n9F31W9+wpNFmLEo/pGvfRCWv8SqH3Wv+DUN3H2AzFTS+5mjsH8WTty0s+A17xZfdMeM9gBNAcXcUJveOb/gnQeJOCH9uiqW3D9Q6zlLiGPWuB5BnbeD/RG6p695Rw1nR75"
    },
    {
      type = "objectgroup",
      id = 3,
      name = "mazeobjects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "index",
      properties = {},
      objects = {
        {
          id = 2,
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 3,
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 704,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 9,
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 10,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "",
          type = "",
          shape = "rectangle",
          x = 896,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 13,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1024,
          y = 320,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 14,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 192,
          width = 64,
          height = 64,
          rotation = 180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 15,
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 288,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 16,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 17,
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 288,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 18,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 19,
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 288,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 20,
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 21,
          name = "",
          type = "",
          shape = "rectangle",
          x = 704,
          y = 288,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 22,
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 23,
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 288,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 24,
          name = "",
          type = "",
          shape = "rectangle",
          x = 896,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 25,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 288,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 26,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1024,
          y = 224,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 27,
          name = "",
          type = "",
          shape = "rectangle",
          x = 896,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 28,
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 29,
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 30,
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 31,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 1056,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 32,
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 33,
          name = "",
          type = "",
          shape = "rectangle",
          x = 672,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 34,
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 35,
          name = "",
          type = "",
          shape = "rectangle",
          x = 864,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 36,
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 37,
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 1024,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 38,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 39,
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 896,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 40,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 41,
          name = "",
          type = "",
          shape = "rectangle",
          x = 416,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 42,
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 1152,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 43,
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 512,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 76,
          visible = true,
          properties = {}
        },
        {
          id = 44,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 832,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 45,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 448,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 46,
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 448,
          width = 64,
          height = 64,
          rotation = -180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 47,
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 704,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 48,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 704,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 49,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 576,
          width = 64,
          height = 64,
          rotation = -270,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 50,
          name = "",
          type = "",
          shape = "rectangle",
          x = 704,
          y = 576,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 51,
          name = "",
          type = "",
          shape = "rectangle",
          x = 704,
          y = 448,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 52,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 448,
          width = 64,
          height = 64,
          rotation = -180,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 53,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 704,
          width = 64,
          height = 64,
          rotation = 270,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 54,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 704,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 55,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 512,
          width = 64,
          height = 64,
          rotation = -270,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 59,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 768,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 61,
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 576,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 63,
          name = "",
          type = "",
          shape = "rectangle",
          x = 960,
          y = 608,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 65,
          name = "",
          type = "",
          shape = "rectangle",
          x = 672,
          y = 704,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 69,
          name = "",
          type = "",
          shape = "rectangle",
          x = 544,
          y = 448,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 73,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 608,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 46,
          visible = true,
          properties = {}
        },
        {
          id = 74,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 416,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 75,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 544,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 76,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 672,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 77,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 800,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 78,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 928,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 79,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 864,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 80,
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 832,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 81,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 288,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 82,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1088,
          y = 1056,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 83,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1024,
          y = 1120,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 38,
          visible = true,
          properties = {}
        },
        {
          id = 84,
          name = "",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 960,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 22,
          visible = true,
          properties = {}
        },
        {
          id = 85,
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 704,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 86,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 576,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 87,
          name = "",
          type = "",
          shape = "rectangle",
          x = 608,
          y = 576,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 88,
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 832,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 89,
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 896,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 126,
          visible = true,
          properties = {}
        },
        {
          id = 90,
          name = "",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 896,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 92,
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 832,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        },
        {
          id = 93,
          name = "",
          type = "",
          shape = "rectangle",
          x = 896,
          y = 896,
          width = 64,
          height = 64,
          rotation = -90,
          gid = 114,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 4,
      name = "ball",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "index",
      properties = {},
      objects = {
        {
          id = 1,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 256,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 129,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
