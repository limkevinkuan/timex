return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.3",
  orientation = "orthogonal",
  renderorder = "right-up",
  width = 18,
  height = 18,
  tilewidth = 20,
  tileheight = 20,
  nextlayerid = 5,
  nextobjectid = 13,
  backgroundcolor = { 64, 64, 64 },
  properties = {
    ["script"] = "MainMenu"
  },
  tilesets = {
    {
      name = "mainmenu",
      firstgid = 1,
      filename = "mainmenu.tsx",
      tilewidth = 100,
      tileheight = 100,
      spacing = 0,
      margin = 0,
      columns = 6,
      image = "mainmenu.png",
      imagewidth = 600,
      imageheight = 100,
      tileoffset = {
        x = -50,
        y = 50
      },
      grid = {
        orientation = "orthogonal",
        width = 100,
        height = 100
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {}
    }
  },
  layers = {
    {
      type = "imagelayer",
      id = 4,
      name = "Image Layer 1",
      visible = true,
      opacity = 1,
      offsetx = 120,
      offsety = 120,
      image = "app_icon_games.png",
      properties = {}
    },
    {
      type = "objectgroup",
      id = 1,
      name = "menu",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "timeguessing",
          type = "",
          shape = "rectangle",
          x = 75,
          y = 230,
          width = 100,
          height = 100,
          rotation = 0,
          visible = true,
          properties = {
            ["buttonaction"] = "startTimeGuessingGame",
            ["script"] = "UIButton"
          }
        },
        {
          id = 2,
          name = "timesetting",
          type = "",
          shape = "rectangle",
          x = 185,
          y = 230,
          width = 100,
          height = 100,
          rotation = 0,
          visible = true,
          properties = {
            ["buttonaction"] = "startTimeSettingGame",
            ["script"] = "UIButton"
          }
        },
        {
          id = 3,
          name = "selectgame",
          type = "",
          shape = "text",
          x = 64,
          y = 64,
          width = 272,
          height = 48,
          rotation = 0,
          visible = false,
          text = "SELECT GAME",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {}
        },
        {
          id = 4,
          name = "janken",
          type = "",
          shape = "rectangle",
          x = 75,
          y = 30,
          width = 100,
          height = 100,
          rotation = 0,
          visible = true,
          properties = {
            ["buttonaction"] = "startJankenGame",
            ["script"] = "UIButton"
          }
        },
        {
          id = 9,
          name = "labyrinth",
          type = "",
          shape = "rectangle",
          x = 15,
          y = 130,
          width = 100,
          height = 100,
          rotation = 0,
          visible = true,
          properties = {
            ["buttonaction"] = "startLabyrinthGame",
            ["script"] = "UIButton"
          }
        }
      }
    },
    {
      type = "objectgroup",
      id = 2,
      name = "icons",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 235,
          y = 280,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 125,
          y = 280,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 125,
          y = 80,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        },
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 65,
          y = 180,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        },
        {
          id = 10,
          name = "",
          type = "",
          shape = "rectangle",
          x = 235,
          y = 80,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "",
          type = "",
          shape = "rectangle",
          x = 295,
          y = 180,
          width = 100,
          height = 100,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
