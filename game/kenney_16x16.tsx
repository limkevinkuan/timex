<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="kenney_16x16" tilewidth="16" tileheight="16" tilecount="6912" columns="64">
 <image source="kenney_16x16.png" width="1024" height="1728"/>
 <tile id="192" type="MazeBall">
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="2" y="2" width="12" height="12">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="211" type="MazeWall"/>
 <tile id="212" type="MazeWall"/>
 <tile id="256" type="MazeKey">
  <properties>
   <property name="keyid" value="yellow"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="4" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="257" type="MazeKey">
  <properties>
   <property name="keyid" value="green"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="4" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="258" type="MazeKey">
  <properties>
   <property name="keyid" value="red"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="4" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="259" type="MazeKey">
  <properties>
   <property name="keyid" value="blue"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="4" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="275" type="MazeWall"/>
 <tile id="276" type="MazeWall"/>
 <tile id="320" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="yellow"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="321" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="green"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="322" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="red"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="323" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="blue"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="384" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureCitrine"/>
   <property name="treasurevalue" type="int" value="200"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="384" duration="375"/>
   <frame tileid="448" duration="125"/>
  </animation>
 </tile>
 <tile id="385" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureEmerald"/>
   <property name="treasurevalue" type="int" value="250"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="385" duration="375"/>
   <frame tileid="449" duration="125"/>
  </animation>
 </tile>
 <tile id="386" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureRuby"/>
   <property name="treasurevalue" type="int" value="500"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="386" duration="375"/>
   <frame tileid="450" duration="125"/>
  </animation>
 </tile>
 <tile id="387" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureDiamond"/>
   <property name="treasurevalue" type="int" value="1000"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="387" duration="375"/>
   <frame tileid="451" duration="125"/>
  </animation>
 </tile>
 <tile id="398" type="MazeWall"/>
 <tile id="448" type="MazeTreasure">
  <properties>
   <property name="treasurevalue" type="int" value="200"/>
  </properties>
 </tile>
 <tile id="449" type="MazeTreasure">
  <properties>
   <property name="treasurevalue" type="int" value="250"/>
  </properties>
 </tile>
 <tile id="450" type="MazeTreasure">
  <properties>
   <property name="treasurevalue" type="int" value="500"/>
  </properties>
 </tile>
 <tile id="451" type="MazeTreasure">
  <properties>
   <property name="treasurevalue" type="int" value="1000"/>
  </properties>
 </tile>
 <tile id="512" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_yellow_round_off"/>
   <property name="buttonontileid" value="button_yellow_round_on"/>
   <property name="name" value="button_yellow_round_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="513" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_green_round_off"/>
   <property name="buttonontileid" value="button_green_round_on"/>
   <property name="name" value="button_green_round_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="514" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_red_round_off"/>
   <property name="buttonontileid" value="button_red_round_on"/>
   <property name="name" value="button_red_round_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="515" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_blue_round_off"/>
   <property name="buttonontileid" value="button_blue_round_on"/>
   <property name="name" value="button_blue_round_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="576" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_yellow_round_off"/>
   <property name="buttonontileid" value="button_yellow_round_on"/>
   <property name="name" value="button_yellow_round_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="577" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_green_round_off"/>
   <property name="buttonontileid" value="button_green_round_on"/>
   <property name="name" value="button_green_round_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="578" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_red_round_off"/>
   <property name="buttonontileid" value="button_red_round_on"/>
   <property name="name" value="button_red_round_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="579" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_blue_round_off"/>
   <property name="buttonontileid" value="button_blue_round_on"/>
   <property name="name" value="button_blue_round_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="580" type="MazeBombFire">
  <properties>
   <property name="name" value="bombfire"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="2" y="2" width="12" height="12">
    <properties>
     <property name="restitution" type="float" value="1"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="640" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_yellow_flat_off"/>
   <property name="buttonontileid" value="button_yellow_flat_on"/>
   <property name="name" value="button_yellow_flat_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="4" y="0" width="8" height="8"/>
   <object id="2" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="641" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_green_flat_off"/>
   <property name="buttonontileid" value="button_green_flat_on"/>
   <property name="name" value="button_green_flat_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="4" y="0" width="8" height="8"/>
   <object id="2" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="642" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_red_flat_off"/>
   <property name="buttonontileid" value="button_red_flat_on"/>
   <property name="name" value="button_red_flat_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="4" y="0" width="8" height="8"/>
   <object id="2" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="643" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_blue_flat_off"/>
   <property name="buttonontileid" value="button_blue_flat_on"/>
   <property name="name" value="button_blue_flat_off"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="4" y="0" width="8" height="8"/>
   <object id="2" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="644" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureBronze"/>
   <property name="treasurevalue" type="int" value="10"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="2" y="2" width="12" height="12">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="645" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureSilver"/>
   <property name="treasurevalue" type="int" value="50"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="2" y="2" width="12" height="12">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="646" type="MazeTreasure">
  <properties>
   <property name="name" value="TreasureGold"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="2" y="2" width="12" height="12">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="653" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="name" value="box_silver_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="654" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="boxunlockedtileid" value="box_silver_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="655" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="name" value="box_silver_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="656" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="boxunlockedtileid" value="box_silver_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="657" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="name" value="box_silver_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="658" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="boxunlockedtileid" value="box_silver_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="704" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_yellow_flat_off"/>
   <property name="buttonontileid" value="button_yellow_flat_on"/>
   <property name="name" value="button_yellow_flat_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="705" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_green_flat_off"/>
   <property name="buttonontileid" value="button_green_flat_on"/>
   <property name="name" value="button_green_flat_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="706" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_red_flat_off"/>
   <property name="buttonontileid" value="button_red_flat_on"/>
   <property name="name" value="button_red_flat_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="707" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="button_blue_flat_off"/>
   <property name="buttonontileid" value="button_blue_flat_on"/>
   <property name="name" value="button_blue_flat_on"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="708" type="MazeBomb">
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="709" type="MazeBomb">
  <properties>
   <property name="name" value="bomb_primed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="709" duration="500"/>
   <frame tileid="708" duration="500"/>
  </animation>
 </tile>
 <tile id="710" type="MazeBomb">
  <properties>
   <property name="name" value="bomb_detonate"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="709" duration="33"/>
   <frame tileid="710" duration="33"/>
  </animation>
 </tile>
 <tile id="717" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="name" value="box_gold_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="718" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="boxunlockedtileid" value="box_gold_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="719" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="name" value="box_gold_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="720" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="boxunlockedtileid" value="box_gold_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="721" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="name" value="box_gold_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="722" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="boxunlockedtileid" value="box_gold_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="774" type="MazeGoal"/>
 <tile id="781" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="name" value="box_iron_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="782" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="boxunlockedtileid" value="box_iron_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="783" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="name" value="box_iron_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="784" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="boxunlockedtileid" value="box_iron_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="785" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="name" value="box_iron_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="786" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="boxunlockedtileid" value="box_iron_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="838" type="MazeGoal"/>
 <tile id="845" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="name" value="box_bronze_!"/>
  </properties>
 </tile>
 <tile id="846" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="boxunlockedtileid" value="box_bronze_!"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="847" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="name" value="box_bronze_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="848" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="boxunlockedtileid" value="box_bronze_coin"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="849" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="name" value="box_bronze_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="850" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="boxunlockedtileid" value="box_bronze_danger"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="902" type="MazeGoal">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="902" duration="125"/>
   <frame tileid="903" duration="125"/>
  </animation>
 </tile>
 <tile id="909" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_!"/>
   <property name="name" value="box_opened_!"/>
  </properties>
 </tile>
 <tile id="910" type="MazeBox"/>
 <tile id="911" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_coin"/>
   <property name="name" value="box_opened_coin"/>
  </properties>
 </tile>
 <tile id="912" type="MazeBox"/>
 <tile id="913" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="box_opened_danger"/>
   <property name="name" value="box_opened_danger"/>
  </properties>
 </tile>
 <tile id="914" type="MazeBox"/>
 <tile id="966" type="MazeGoal">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="966" duration="125"/>
   <frame tileid="967" duration="125"/>
  </animation>
 </tile>
 <tile id="981" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_opened"/>
   <property name="static" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="982" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_standard"/>
   <property name="static" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="983" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_damaged"/>
   <property name="static" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="984" type="MazeBox">
  <properties>
   <property name="boxdamagetileid" value="crate_damaged"/>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_reinforced"/>
   <property name="static" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1045" type="MazeWall"/>
 <tile id="1046" type="MazeWall"/>
 <tile id="1047" type="MazeWall"/>
 <tile id="1048" type="MazeWall"/>
 <tile id="1090" type="MazeSpring">
  <properties>
   <property name="name" value="spring_in"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="8" width="16" height="8"/>
  </objectgroup>
 </tile>
 <tile id="1091" type="MazeSpring"/>
 <tile id="1092" type="MazeSpring">
  <properties>
   <property name="name" value="spring_out"/>
  </properties>
  <animation>
   <frame tileid="1092" duration="67"/>
   <frame tileid="1091" duration="67"/>
   <frame tileid="1090" duration="67"/>
  </animation>
 </tile>
 <tile id="1093">
  <objectgroup draworder="index">
   <object id="2" type="CollisionFixture" x="7" y="0" width="2" height="16">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="1109" type="MazeWall"/>
 <tile id="1110" type="MazeWall"/>
 <tile id="1111" type="MazeWall"/>
 <tile id="1112" type="MazeWall"/>
 <tile id="1220">
  <animation>
   <frame tileid="1220" duration="125"/>
   <frame tileid="1221" duration="125"/>
  </animation>
 </tile>
 <tile id="1802" type="MazeWall"/>
 <tile id="1803" type="MazeWall"/>
 <tile id="1866" type="MazeWall"/>
 <tile id="1867" type="MazeWall"/>
 <tile id="1930" type="MazeWall"/>
 <tile id="1931" type="MazeWall"/>
 <tile id="1994" type="MazeWall"/>
 <tile id="1995" type="MazeWall"/>
 <tile id="2058" type="MazeWall"/>
 <tile id="2059" type="MazeWall"/>
</tileset>
