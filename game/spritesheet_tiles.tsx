<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.5" name="spritesheet_tiles" tilewidth="64" tileheight="64" tilecount="128" columns="8">
 <tileoffset x="-32" y="32"/>
 <image source="spritesheet_tiles.png" width="512" height="1024"/>
 <tile id="0" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="dangerbox"/>
   <property name="boxopenedtileid" value="dangerboxopened"/>
   <property name="name" value="dangerboxopened"/>
  </properties>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="22"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="124" duration="100"/>
  </animation>
 </tile>
 <tile id="6" type="MazeBomb">
  <properties>
   <property name="name" value="bomb_detonate"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="6" duration="66"/>
   <frame tileid="14" duration="66"/>
   <frame tileid="22" duration="66"/>
   <frame tileid="30" duration="66"/>
   <frame tileid="38" duration="66"/>
  </animation>
 </tile>
 <tile id="8" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="dangerbox"/>
   <property name="boxopenedtileid" value="dangerboxopened"/>
   <property name="name" value="dangerbox"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="12" type="MazeWall">
  <properties>
   <property name="breakable" value="bomb"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="20" type="MazeWall"/>
 <tile id="21" type="MazeTreasure">
  <properties>
   <property name="name" value="silver"/>
   <property name="pickupsound1" type="file" value="snd/coin1.ogg"/>
   <property name="pickupsound2" type="file" value="snd/coin2.ogg"/>
   <property name="pickupsound3" type="file" value="snd/coin3.ogg"/>
   <property name="pickupsound4" type="file" value="snd/coin4.ogg"/>
   <property name="pickupsound5" type="file" value="snd/coin5.ogg"/>
   <property name="pickupsound6" type="file" value="snd/coin6.ogg"/>
   <property name="pickupsound7" type="file" value="snd/coin7.ogg"/>
   <property name="pickupsound8" type="file" value="snd/coin8.ogg"/>
   <property name="treasurevalue" type="int" value="50"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="24" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_damaged"/>
   <property name="static" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="25">
  <animation>
   <frame tileid="25" duration="100"/>
   <frame tileid="33" duration="100"/>
  </animation>
 </tile>
 <tile id="28" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="specialboxstatic"/>
   <property name="boxopenedtileid" value="specialboxstaticopened"/>
   <property name="name" value="specialboxstaticopened"/>
  </properties>
 </tile>
 <tile id="29">
  <animation>
   <frame tileid="29" duration="100"/>
   <frame tileid="117" duration="100"/>
  </animation>
 </tile>
 <tile id="32" type="MazeBox">
  <properties>
   <property name="boxdamagetileid" value="crate_damaged"/>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="name" value="crate_reinforced"/>
   <property name="static" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="33">
  <animation>
   <frame tileid="33" duration="100"/>
   <frame tileid="25" duration="100"/>
  </animation>
 </tile>
 <tile id="36" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="specialbox"/>
   <property name="boxopenedtileid" value="specialboxopened"/>
   <property name="name" value="specialboxopened"/>
  </properties>
 </tile>
 <tile id="37" type="MazeTreasure">
  <properties>
   <property name="name" value="copper"/>
   <property name="pickupsound1" type="file" value="snd/coin1.ogg"/>
   <property name="pickupsound2" type="file" value="snd/coin2.ogg"/>
   <property name="pickupsound3" type="file" value="snd/coin3.ogg"/>
   <property name="pickupsound4" type="file" value="snd/coin4.ogg"/>
   <property name="pickupsound5" type="file" value="snd/coin5.ogg"/>
   <property name="pickupsound6" type="file" value="snd/coin6.ogg"/>
   <property name="pickupsound7" type="file" value="snd/coin7.ogg"/>
   <property name="pickupsound8" type="file" value="snd/coin8.ogg"/>
   <property name="treasurevalue" type="int" value="25"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="40" type="MazeBox">
  <properties>
   <property name="boxopenedtileid" value="crate_opened"/>
   <property name="collidable" type="bool" value="false"/>
   <property name="name" value="crate_opened"/>
   <property name="static" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="41" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="yellowbutton"/>
   <property name="buttonontileid" value="yellowbuttonpressed"/>
   <property name="name" value="yellowbuttonpressed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="44" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="specialboxstatic"/>
   <property name="boxopenedtileid" value="specialboxstaticopened"/>
   <property name="name" value="specialboxstatic"/>
  </properties>
 </tile>
 <tile id="45" type="MazeTreasure">
  <properties>
   <property name="name" value="star"/>
   <property name="pickupsound1" type="file" value="snd/star.ogg"/>
   <property name="treasurevalue" type="int" value="1000"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="46" type="MazeBombFire">
  <properties>
   <property name="name" value="bombfire"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="46" duration="66"/>
   <frame tileid="54" duration="66"/>
   <frame tileid="62" duration="66"/>
   <frame tileid="70" duration="66"/>
   <frame tileid="78" duration="66"/>
  </animation>
 </tile>
 <tile id="48" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="coinboxstatic"/>
   <property name="boxopenedtileid" value="coinboxstaticopened"/>
   <property name="name" value="coinboxstaticopened"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="49" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="yellowbutton"/>
   <property name="buttonontileid" value="yellowbuttonpressed"/>
   <property name="name" value="yellowbutton"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="10" y="22" width="42" height="42">
    <ellipse/>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="52" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="specialbox"/>
   <property name="boxopenedtileid" value="specialboxopened"/>
   <property name="name" value="specialbox"/>
   <property name="static" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="53" type="MazeKey">
  <properties>
   <property name="keyid" value="yellowkey"/>
   <property name="name" value="yellowkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="12" y="20" width="40" height="24">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="56" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="coinbox"/>
   <property name="boxopenedtileid" value="coinboxopened"/>
   <property name="name" value="coinboxopened"/>
   <property name="static" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="57" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="redbutton"/>
   <property name="buttonontileid" value="redbuttonpressed"/>
   <property name="name" value="redbuttonpressed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="61" type="MazeKey">
  <properties>
   <property name="keyid" value="redkey"/>
   <property name="name" value="redkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="12" y="20" width="40" height="24">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="64" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="coinboxstatic"/>
   <property name="boxopenedtileid" value="coinboxstaticopened"/>
   <property name="dropitemsound" type="file" value="snd/dropcoins.ogg"/>
   <property name="name" value="coinboxstatic"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="65" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="redbutton"/>
   <property name="buttonontileid" value="redbuttonpressed"/>
   <property name="name" value="redbutton"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="10" y="22" width="42" height="42">
    <ellipse/>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="69" type="MazeKey">
  <properties>
   <property name="keyid" value="greenkey"/>
   <property name="name" value="greenkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="12" y="20" width="40" height="24">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="72" type="MazeBox">
  <properties>
   <property name="boxclosedtileid" value="coinbox"/>
   <property name="boxopenedtileid" value="coinboxopened"/>
   <property name="dropitemsound" type="file" value="snd/dropcoins.ogg"/>
   <property name="name" value="coinbox"/>
   <property name="static" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="73" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="greenbutton"/>
   <property name="buttonontileid" value="greenbuttonpressed"/>
   <property name="name" value="greenbuttonpressed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="74" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="yellowkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="75" type="MazeGoal">
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="30" y="0" width="4" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="76">
  <animation>
   <frame tileid="76" duration="100"/>
   <frame tileid="84" duration="100"/>
  </animation>
 </tile>
 <tile id="77" type="MazeKey">
  <properties>
   <property name="keyid" value="bluekey"/>
   <property name="name" value="bluekey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="12" y="20" width="40" height="24">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="80" type="MazeBomb">
  <properties>
   <property name="name" value="bomb_primed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="6" y="6" width="52" height="52">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="80" duration="100"/>
   <frame tileid="88" duration="400"/>
  </animation>
 </tile>
 <tile id="81" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="greenbutton"/>
   <property name="buttonontileid" value="greenbuttonpressed"/>
   <property name="name" value="greenbutton"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="10" y="22" width="42" height="42">
    <ellipse/>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="82" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="redkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="84">
  <animation>
   <frame tileid="84" duration="100"/>
   <frame tileid="76" duration="100"/>
  </animation>
 </tile>
 <tile id="85" type="MazeTreasure">
  <properties>
   <property name="name" value="citrine"/>
   <property name="treasurevalue" type="int" value="200"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="88" type="MazeBomb">
  <properties>
   <property name="name" value="bomb"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="6" y="6" width="52" height="52">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="89" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="bluebutton"/>
   <property name="buttonontileid" value="bluebuttonpressed"/>
   <property name="name" value="bluebuttonpressed"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="90" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="greenkey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="93" type="MazeTreasure">
  <properties>
   <property name="name" value="ruby"/>
   <property name="treasurevalue" type="int" value="400"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="97" type="MazeButton">
  <properties>
   <property name="buttonofftileid" value="bluebutton"/>
   <property name="buttonontileid" value="bluebuttonpressed"/>
   <property name="name" value="bluebutton"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="10" y="22" width="42" height="42">
    <ellipse/>
   </object>
   <object id="2" type="CollisionFixture" x="0" y="50" width="64" height="14">
    <properties>
     <property name="reacttocollision" type="bool" value="false"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="98" type="MazeLock">
  <properties>
   <property name="lockkeyid" value="bluekey"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="0" width="64" height="64"/>
  </objectgroup>
 </tile>
 <tile id="100">
  <animation>
   <frame tileid="100" duration="100"/>
   <frame tileid="108" duration="100"/>
  </animation>
 </tile>
 <tile id="101" type="MazeTreasure">
  <properties>
   <property name="name" value="emerald"/>
   <property name="treasurevalue" type="int" value="600"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="105" type="MazeSpring">
  <properties>
   <property name="name" value="spring_out"/>
  </properties>
  <animation>
   <frame tileid="105" duration="100"/>
  </animation>
 </tile>
 <tile id="108">
  <animation>
   <frame tileid="108" duration="100"/>
   <frame tileid="100" duration="100"/>
  </animation>
 </tile>
 <tile id="109" type="MazeTreasure">
  <properties>
   <property name="name" value="sapphire"/>
   <property name="treasurevalue" type="int" value="800"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113" type="MazeSpring">
  <properties>
   <property name="name" value="spring_in"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="12" y="30" width="40" height="4">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="117">
  <animation>
   <frame tileid="117" duration="100"/>
   <frame tileid="29" duration="100"/>
  </animation>
 </tile>
 <tile id="123">
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="0" y="1" width="64" height="16"/>
  </objectgroup>
 </tile>
 <tile id="124">
  <animation>
   <frame tileid="124" duration="100"/>
   <frame tileid="5" duration="100"/>
  </animation>
 </tile>
 <tile id="125" type="MazeTreasure">
  <properties>
   <property name="name" value="gold"/>
   <property name="pickupsound1" type="file" value="snd/coin1.ogg"/>
   <property name="pickupsound2" type="file" value="snd/coin2.ogg"/>
   <property name="pickupsound3" type="file" value="snd/coin3.ogg"/>
   <property name="pickupsound4" type="file" value="snd/coin4.ogg"/>
   <property name="pickupsound5" type="file" value="snd/coin5.ogg"/>
   <property name="pickupsound6" type="file" value="snd/coin6.ogg"/>
   <property name="pickupsound7" type="file" value="snd/coin7.ogg"/>
   <property name="pickupsound8" type="file" value="snd/coin8.ogg"/>
   <property name="treasurevalue" type="int" value="100"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" type="CollisionFixture" x="16" y="16" width="32" height="32">
    <properties>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
