local levity = require "levity"
for firstmapfile in love.filesystem.lines("gamefiles") do
	levity:setNextMap(firstmapfile)
	break
end
love.filesystem.setRequirePath(
	"scr/?.lua;"..
	love.filesystem.getRequirePath())
levity:setSystemFont(nil, 24)
levity.showversion = false

function levity.initDefaultPrefs(dp)
	dp.canvasscaleint = false
	dp.canvasscalesoft = true
end

function exitCurrentGame()
	if love.filesystem.exists("mainmenu.lua") then
		levity:setNextMap("mainmenu.lua")
	else
		love.event.quit()
	end
end
