return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.3",
  orientation = "orthogonal",
  renderorder = "right-up",
  width = 50,
  height = 50,
  tilewidth = 16,
  tileheight = 16,
  nextlayerid = 5,
  nextobjectid = 20,
  properties = {
    ["script"] = "JankenGame"
  },
  tilesets = {
    {
      name = "janken",
      firstgid = 1,
      filename = "janken.tsx",
      tilewidth = 80,
      tileheight = 80,
      spacing = 0,
      margin = 0,
      columns = 3,
      image = "janken.png",
      imagewidth = 240,
      imageheight = 80,
      tileoffset = {
        x = 0,
        y = 80
      },
      grid = {
        orientation = "orthogonal",
        width = 80,
        height = 80
      },
      properties = {
        ["commoncollision"] = 0
      },
      terrains = {},
      tilecount = 3,
      tiles = {
        {
          id = 0,
          properties = {
            ["name"] = "rock"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "ellipse",
                x = 10,
                y = 10,
                width = 60,
                height = 60,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          }
        },
        {
          id = 1,
          properties = {
            ["name"] = "scissors"
          }
        },
        {
          id = 2,
          properties = {
            ["name"] = "paper"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "objectgroup",
      id = 1,
      name = "waiting",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 18,
          name = "camera",
          type = "",
          shape = "rectangle",
          x = 472,
          y = 472,
          width = 256,
          height = 256,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 19,
          name = "title",
          type = "",
          shape = "text",
          x = 512,
          y = 528,
          width = 176,
          height = 144,
          rotation = 0,
          visible = true,
          text = "Waiting for opponent",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 2,
      name = "selection",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "rock",
          type = "",
          shape = "rectangle",
          x = 104,
          y = 104,
          width = 80,
          height = 80,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {
            ["buttonevent"] = "inputRock",
            ["script"] = "UIButton"
          }
        },
        {
          id = 2,
          name = "scissors",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 216,
          width = 80,
          height = 80,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {
            ["buttonevent"] = "inputScissors",
            ["script"] = "UIButton"
          }
        },
        {
          id = 3,
          name = "paper",
          type = "",
          shape = "rectangle",
          x = 216,
          y = 104,
          width = 80,
          height = 80,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {
            ["buttonevent"] = "inputPaper",
            ["script"] = "UIButton"
          }
        },
        {
          id = 7,
          name = "camera",
          type = "",
          shape = "rectangle",
          x = 72,
          y = 72,
          width = 256,
          height = 256,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 3,
      name = "opponents",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 13,
          name = "camera",
          type = "",
          shape = "rectangle",
          x = 72,
          y = 472,
          width = 256,
          height = 256,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 14,
          name = "opponent1",
          type = "",
          shape = "text",
          x = 96,
          y = 544,
          width = 208,
          height = 48,
          rotation = 0,
          visible = true,
          text = "Luke",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {
            ["buttonevent"] = "waitForOpponent1",
            ["script"] = "UIButton"
          }
        },
        {
          id = 15,
          name = "opponent3",
          type = "",
          shape = "text",
          x = 96,
          y = 640,
          width = 208,
          height = 48,
          rotation = 0,
          visible = true,
          text = "Elizabeth",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {
            ["buttonevent"] = "waitForOpponent3",
            ["script"] = "UIButton"
          }
        },
        {
          id = 16,
          name = "opponent2",
          type = "",
          shape = "text",
          x = 96,
          y = 592,
          width = 208,
          height = 48,
          rotation = 0,
          visible = true,
          text = "Nathan",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {
            ["buttonevent"] = "waitForOpponent2",
            ["script"] = "UIButton"
          }
        },
        {
          id = 17,
          name = "title",
          type = "",
          shape = "text",
          x = 72,
          y = 496,
          width = 256,
          height = 48,
          rotation = 0,
          visible = true,
          text = "Opponents",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 4,
      name = "result",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "index",
      properties = {},
      objects = {
        {
          id = 9,
          name = "opponent",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 80,
          width = 80,
          height = 80,
          rotation = -180,
          gid = 2,
          visible = true,
          properties = {
            ["script"] = "JankenPlayer"
          }
        },
        {
          id = 8,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 560,
          y = 320,
          width = 80,
          height = 80,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {
            ["script"] = "JankenPlayer"
          }
        },
        {
          id = 10,
          name = "camera",
          type = "",
          shape = "rectangle",
          x = 472,
          y = 72,
          width = 256,
          height = 256,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "result",
          type = "",
          shape = "text",
          x = 472,
          y = 112,
          width = 256,
          height = 32,
          rotation = 0,
          visible = true,
          text = "WIN!",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {}
        },
        {
          id = 12,
          name = "shake",
          type = "",
          shape = "text",
          x = 520,
          y = 184,
          width = 160,
          height = 32,
          rotation = 0,
          visible = true,
          text = "SHAKE",
          fontfamily = "Liberation Sans",
          pixelsize = 36,
          wrap = true,
          color = { 255, 255, 255 },
          bold = true,
          halign = "center",
          properties = {
            ["textalign"] = "left"
          }
        }
      }
    }
  }
}
