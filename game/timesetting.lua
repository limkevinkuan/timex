return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.4",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 20,
  height = 20,
  tilewidth = 18,
  tileheight = 18,
  nextlayerid = 6,
  nextobjectid = 7,
  properties = {
    ["script"] = "TimeSettingGame"
  },
  tilesets = {
    {
      name = "timesetting_hands",
      firstgid = 1,
      filename = "timesetting_hands.tsx",
      tilewidth = 30,
      tileheight = 192,
      spacing = 0,
      margin = 0,
      columns = 2,
      image = "timesetting_hands.png",
      imagewidth = 60,
      imageheight = 192,
      tileoffset = {
        x = -15,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 30,
        height = 192
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {
        {
          id = 0,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 5,
                y = 0,
                width = 20,
                height = 150,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = false
                }
              }
            }
          }
        },
        {
          id = 1,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 5,
                y = 60,
                width = 20,
                height = 90,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = false
                }
              }
            }
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      id = 1,
      name = "face",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "timesetting_watchface.png",
      properties = {}
    },
    {
      type = "objectgroup",
      id = 2,
      name = "hands",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "index",
      properties = {},
      objects = {
        {
          id = 2,
          name = "minute",
          type = "",
          shape = "rectangle",
          x = 180,
          y = 180,
          width = 30,
          height = 192,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        },
        {
          id = 3,
          name = "hour",
          type = "",
          shape = "rectangle",
          x = 180,
          y = 180,
          width = 30,
          height = 192,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 3,
      name = "quiz",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 4,
          name = "question",
          type = "",
          shape = "text",
          x = 72,
          y = 216,
          width = 216,
          height = 36,
          rotation = 0,
          visible = true,
          text = "12:15?",
          fontfamily = "Gotham",
          pixelsize = 36,
          wrap = true,
          color = { 24, 24, 24 },
          bold = true,
          halign = "center",
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 5,
      name = "instructions",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {
        ["backcolor"] = "#80a00000",
        ["script"] = "Instructions"
      },
      objects = {
        {
          id = 5,
          name = "instructions",
          type = "",
          shape = "text",
          x = 36,
          y = 36,
          width = 288,
          height = 288,
          rotation = 0,
          visible = true,
          text = "Move the hands to set the clock to the right time!\n\nReady?",
          fontfamily = "Gotham",
          pixelsize = 36,
          wrap = true,
          bold = true,
          halign = "center",
          valign = "center",
          properties = {}
        }
      }
    }
  }
}
