#!/bin/bash

LOVE_ANDROID_ASSET_PATH=love-android-sdl2/app/src/main/assets
GAME_ASSET=${GAME_ASSET:=game.love}

./make-game.sh
mkdir -p $LOVE_ANDROID_ASSET_PATH
cp $GAME_ASSET $LOVE_ANDROID_ASSET_PATH
